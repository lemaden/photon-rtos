/*
 * PhotonRTOS础光实时操作系统 -- 多核调度
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/cpu.h>
#include <photon/cpumask.h>
#include <photon/init.h>
#include <photon/sched.h>
#include <photon/smp.h>

#include <asm/asm-offsets.h>

#include <autosar/internal.h>

void (*__smp_raise_ipi)(const struct cpumask *, uint32_t);
static void (*smp_ioc_cb)(void);

void smp_set_ioc_isr(void (*fn)(void))
{
	smp_ioc_cb = fn;
}

void smp_set_raise_ipi_call(void (*fn)(const struct cpumask *, uint32_t))
{
	__smp_raise_ipi = fn;
}

/**
 * 触发IPI中断
 */
void arch_raise_ipi(const struct cpumask *mask, enum ipi_cmd_type cmd_type)
{
	__smp_raise_ipi(mask, cmd_type);
}

/**
 * 处理IPI中断
 */
void do_IPI(int32_t ipinr, struct exception_spot *regs)
{
	switch (ipinr) {
	case IPI_SHUTDOWN:
		/* 关闭核心 */
		autosar_shutdown_core();
		break;
	case IPI_IOC:
		/* 触发IOC回调 */
		smp_ioc_cb();
		break;
	case IPI_ASYNC:
		os_check_async_action();
		break;
	default:
		pr_err("unhanded IPI nr %d", ipinr);
		break;
	}
}
