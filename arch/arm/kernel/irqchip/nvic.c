/*
 * PhotonRTOS础光实时操作系统 -- nvic中断控制器文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/irq.h>

#if defined(CONFIG_ARM_NVIC)

struct irq_controller os_irq_controller = {
	.set_trigger_type = NULL,
	.mask = NULL,
	.unmask = NULL,
	.ack = NULL,
	.clear_pending = NULL,
	.get_status = NULL,
	.set_priority = NULL,
	.get_priority = NULL,
};
#endif