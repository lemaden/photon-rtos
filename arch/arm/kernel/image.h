/*
 * PhotonRTOS础光实时操作系统
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_IMAGE_H
#define __ASM_IMAGE_H

#ifndef LINKER_SCRIPT
#error This file should only be included in photon.lds.S
#endif


#define DATA_LE64(data) ((data) & 0xffffffffffffffff)

#define __HEAD_FLAG_BE	0

#define __HEAD_FLAGS	(__HEAD_FLAG_BE << 0)

/*
 * These will output as part of the Image header, which should be little-endian
 * regardless of the endianness of the kernel. While constant values could be
 * endian swapped in head.S, all are done here for consistency.
 */
#define HEAD_SYMBOLS									\
	_kernel_size_le		= DATA_LE64(kernel_text_end - kernel_text_start);	\
	_kernel_offset_le	= DATA_LE64(TEXT_OFFSET);				\
	_kernel_flags_le	= DATA_LE64(__HEAD_FLAGS);

#endif /* __ASM_IMAGE_H */
