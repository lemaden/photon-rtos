/*
 * PhotonRTOS础光实时操作系统 -- 体系架构定时器
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_ARCH_TIMER_H
#define __ASM_ARCH_TIMER_H

#include <photon/bug_build.h>
#include <photon/init.h>
#include <asm/types.h>

#include <asm/arm_arch_timer.h>
#include <asm/barrier.h>

#ifdef CONFIG_CPU_CORTEX_M
#define ARCH_FUN_arch_timer_get_cntfrq_DEFINED
static inline uint32_t arch_timer_get_cntfrq(void)
{
	return (200*1024*1024);
}

static inline uint32_t arch_counter_get_cntvct(void)
{
	return 0;
}

static uint64_t arch_cycles_to_usecs(const uintptr_t cycles)
{
	return cycles / 1000;
}

#endif

#ifdef CONFIG_CPU_CORTEX_R
/**
 * arch_timer_get_cntfrq
 *
 * @return uint32_t
 */
#define ARCH_FUN_arch_timer_get_cntfrq_DEFINED
static inline uint32_t arch_timer_get_cntfrq(void)
{
	/* FIXME: get freq from p15? */
	return (150*1024*1024);
}

static inline uint32_t arch_counter_get_cntvct(void)
{
	uint32_t cval;

	isb();
	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r" (cval));
	return cval;
}

static uint64_t arch_cycles_to_usecs(const uintptr_t cycles)
{
	return cycles / 1000;
}

#endif

#endif /* __ASM_ARCH_TIMER_H */
