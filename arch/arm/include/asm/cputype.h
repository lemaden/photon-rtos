/*
 * PhotonRTOS础光实时操作系统 -- CPU头
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_CPUTYPE_H
#define __ASM_CPUTYPE_H

#ifndef __ASSEMBLY__
#ifdef CONFIG_CPU_CORTEX_R
#define read_ctr() ({							\
	uint32_t __val;							\
	asm("mrc p15, 1, %0, c0,c0, 0" : "=r" (__val));			\
	__val;								\
})
#endif

#ifdef CONFIG_CPU_CORTEX_M
	#define read_ctr() 0
#endif


static inline uint32_t __attribute_const__ read_cpuid_cachetype(void)
{
	return read_ctr();
}

#ifdef CONFIG_CPU_CORTEX_R
#define read_cpuid() ({							\
	uint32_t __val;							\
	asm("mrc p15, 0, %0, c0, c0, 5" : "=r" (__val));		\
	__val;								\
})/* Read MPIDR register */
#endif

#ifdef CONFIG_CPU_CORTEX_M
	#define read_cpuid() 0
#endif

#endif

#endif
