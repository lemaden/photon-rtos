/*
 * PhotonRTOS础光实时操作系统 -- 多核
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_SMP_H

#define __ASM_SMP_H

#ifndef __ASSEMBLY__
#include <photon/process.h>
#include <asm/exception.h>

struct cpumask;
enum ipi_cmd_type;

#define ARCH_FUN_smp_processor_id_DEFINED
#define smp_processor_id() (current_proc_info()->cpu)

/**
 * 用于触发IPI的函数
 * 由硬件驱动设置
 */
extern void (*__smp_raise_ipi)(const struct cpumask *, uint32_t);
typedef void (*raise_ipi_call_fn)(const struct cpumask *, uint32_t);
/**
 * 硬件调用此函数，设置触IPI的函数指针
 */
#define ARCH_FUN_smp_set_raise_ipi_call_DEFINED
void smp_set_raise_ipi_call(raise_ipi_call_fn fn);
void smp_set_ioc_isr(void (*fn)(void));

/**
 * 向其他核发送IPI中断
 * 体系架构相关
 */
#define ARCH_FUN_arch_raise_ipi_DEFINED
void arch_raise_ipi(const struct cpumask *mask, enum ipi_cmd_type cmd_type);

/**
 * IPI中断处理函数
 */
#define ARCH_FUN_do_IPI_DEFINED
void do_IPI(int32_t ipinr, struct exception_spot *regs);

extern void arch_start_slave(struct slave_cpu_data *slave_cpu_data);

static int arch_get_core_id(void)
{
	return 0;
}

#endif /* __ASSEMBLY__ */

#endif /* __ASM_SMP_H */
