/*
 * PhotonRTOS础光实时操作系统 -- 类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/types.h>
