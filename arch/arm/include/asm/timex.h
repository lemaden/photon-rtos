/*
 * PhotonRTOS础光实时操作系统 -- 时间戳
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_TIMEX_H
#define __ASM_TIMEX_H

#include <asm/arch_timer.h>

/**
 * 获取时间戳
 */
#if defined(CONFIG_CPU_CORTEX_R)
#define get_cycles()	arch_counter_get_cntvct()
#endif

#define ARCH_FUN_get_cycles_DEFINED

#endif
