/*
 * PhotonRTOS础光实时操作系统 -- section头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/sections.h>

#define READABLE   1 << 0
#define WRITABLE   1 << 1
#define EXECUTABLE 1 << 2