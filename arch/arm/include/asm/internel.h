/*
 * PhotonRTOS础光实时操作系统 -- 架构内部头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_INTERNEL_H
#define __ASM_INTERNEL_H

#include <asm/types.h>

struct task_desc;

/* kernel/irq.c */
#define ARCH_FUN_arch_switch_cpu_context_DEFINED
asmlinkage void arch_switch_cpu_context(struct task_desc *prev,
	struct task_desc *next);

/* kernel/setup.c */
#define ARCH_FUN_start_arch_DEFINED
#define ARCH_FUN_init_time_DEFINED
void start_arch(void);
void init_time(void);

/* kernel/traps.c */
#define ARCH_FUN_dump_task_stack_DEFINED
void dump_task_stack(struct task_desc *tsk, uintptr_t *sp);

/* kernel/cpu.c */
#define ARCH_FUN_arch_launch_cpu_DEFINED
int32_t arch_launch_cpu(uint32_t cpu);

void arch_timer_secondary_init(void);

#endif /* __ASM_INTERNEL_H */
