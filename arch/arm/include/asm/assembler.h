/*
 * PhotonRTOS础光实时操作系统 -- 汇编相关
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef __ASM_ASSEMBLER_H__
#define __ASM_ASSEMBLER_H__

#ifndef __ASSEMBLY__
#error "Only include this from assembly code"
#endif

#define __PHOTON_ARM_ARCH__ 7

#define __ALIGN		.align 0
#define __ALIGN_STR	".align 0"

/*
 * SMP数据内存屏障
 */
	.macro	smp_dmb mode
#ifdef CONFIG_SMP
#if __PHOTON_ARM_ARCH__ >= 7
	.ifeqs "\mode", "arm"
	ALT_SMP(dmb	ish)
	.else
	ALT_SMP(W(dmb)	ish)
	.endif
#elif __PHOTON_ARM_ARCH__ == 6
	ALT_SMP(mcr	p15, 0, r0, c7, c10, 5)	@ dmb
#else
#error Incompatible SMP platform
#endif
	.ifeqs "\mode", "arm"
	ALT_UP(nop)
	.else
	ALT_UP(W(nop))
	.endif
#endif
	.endm

/* Some toolchains use other characters (e.g. '`') to mark new line in macro */
#ifndef ASM_NL
#define ASM_NL			;
#endif

#ifdef __ASSEMBLY__

#ifndef LINKER_SCRIPT
#define ALIGN __ALIGN
#define ALIGN_STR __ALIGN_STR

#ifndef ENTRY
#define ENTRY(name)		\
	.globl name ASM_NL	\
	ALIGN ASM_NL		\
name :
#endif
#endif /* LINKER_SCRIPT */

#ifndef END
#define END(name)		\
	.size name, . -name
#endif

#define ENDPROC(name)		\
	.type name, %function;	\
	END(name)

	.irp	c,,eq,ne,cs,cc,mi,pl,vs,vc,hi,ls,ge,lt,gt,le,hs,lo
	.macro	ret\c, reg
#if __PHOTON_ARM_ARCH__ < 6
	mov\c	pc, \reg
#else
	.ifeqs	"\reg", "lr"
	bx\c	\reg
	.else
	mov\c	pc, \reg
	.endif
#endif
	.endm
	.endr
#endif
#endif /* __ASM_ASSEMBLER_H__ */
