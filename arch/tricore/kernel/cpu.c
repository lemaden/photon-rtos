/*
 * PhotonRTOS础光实时操作系统 -- CPU初始化相关文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/init.h>
#include <asm/types.h>
#include <photon/cpu.h>

#include <asm/processor.h>

#include <IfxSrc_Reg.h>
#include <std/IfxSrc.h>

#define CRL_APB_RST_LPD_TOP_OFFSET              0XFF5E023C
#define RPU0_TCM_CACHE_BASE			0xffe00000
#define RPU1_TCM_CACHE_BASE			0xffe90000
#define RPU1_ATCM_SIZE                          0x00002000


int32_t arch_launch_cpu(uint32_t cpu)
{
	return 0;
}

void arch_start_slave(struct slave_cpu_data *slave_cpu_data)
{
	return;
}
