/*
 * PhotonRTOS础光实时操作系统 -- 栈追溯
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/kernel.h>
#include <photon/sched.h>
#include <photon/sections.h>
#include <asm/internel.h>


void dump_task_stack(struct task_desc *tsk, uintptr_t *sp)
{
//	dump_backtrace(NULL, tsk);
	barrier();
}


