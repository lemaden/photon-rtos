/*
 * PhotonRTOS础光实时操作系统 -- ARM架构一类中断相关文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#include <asm/irq.h>

#include <asm/isr1.h>
#include <photon/printk.h>
/**
 * 每一个中断都有对应的weak函数
 * 这里只提供目前设置为一类中断的中断号
 * 并提供一个默认实现
*/
ISR1_DEFAULT_FUNC(72);
ISR1_DEFAULT_FUNC(72) {
        printk("isr1调用默认实现！\n");
}



__interrupt ISR1(isr1) = {
        [72] = isr1_default_func72,
        NULL,
};
