/*
 * PhotonRTOS础光实时操作系统 -- tc397中断处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/irq.h>
#include <photon/init.h>
#include <photon/printk.h>
#include <photon/smp_lock.h>
#include <photon/smp.h>
#include <asm/types.h>

int32_t gic_set_type(uint32_t hw_irq, uint32_t type)
{
	return 0;
}

static void tc397_mask_irq(uint32_t hw_irq)
{

}

static void tc397_unmask_irq(uint32_t hw_irq)
{

}

static void tc397_eoi_irq(uint32_t hw_irq)
{
	Ifx_CPU_ICR icr;

	icr.U = __mfcr(CPU_ICR);    /*Fetch the ICR value */
	icr.B.CCPN = 0;

	__mtcr(CPU_ICR, icr.U);

}

static void tc397_clear_pending(uint32_t hw_irq)
{

}

static uint32_t tc397_get_irq_status(uint32_t hw_irq)
{
	return 0;
}

void irq_controller_init()
{
	return;
}

struct irq_controller os_irq_controller = {
	.set_trigger_type = gic_set_type,
	.mask = tc397_mask_irq,
	.unmask = tc397_unmask_irq,
	.ack = tc397_eoi_irq,
	.clear_pending = tc397_clear_pending,
	.get_status = tc397_get_irq_status,
};
