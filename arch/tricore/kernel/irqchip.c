/*
 * PhotonRTOS础光实时操作系统 -- 中断相关文件
 *
 * Copyright (C) 2022, 2023, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#include <photon/irq.h>
#include <photon/init.h>

void init_irq_controller(void)
{
	irq_controller_init();
}
