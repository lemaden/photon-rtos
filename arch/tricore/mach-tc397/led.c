/*
 * PhotonRTOS础光实时操作系统 -- Led驱动
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "IfxPort.h"

#define LED_D107    &MODULE_P33,4

#define LED_D108    &MODULE_P33,6

#define LED_D109    &MODULE_P20,11

#define LED_D110    &MODULE_P20,12

#define LED_D111	&MODULE_P20,14


/* Function to initialize the LED */
void initLED(void)
{
    IfxPort_setPinMode(LED_D107, IfxPort_Mode_outputPushPullGeneral);    /* Initialize LED port pin                      */
    IfxPort_setPinMode(LED_D108, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(LED_D107, IfxPort_State_high);                   /* Turn off LED (LED is low-level active)       */
    IfxPort_setPinState(LED_D108, IfxPort_State_high);                   /* Turn off LED (LED is low-level active)       */

    IfxPort_setPinMode(LED_D109, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(LED_D109, IfxPort_State_high);                   /* Turn off LED (LED is low-level active)       */

    IfxPort_setPinMode(LED_D110, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(LED_D110, IfxPort_State_high);                   /* Turn off LED (LED is low-level active)       */

    IfxPort_setPinMode(LED_D111, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(LED_D111, IfxPort_State_high);                   /* Turn off LED (LED is low-level active)       */

}

void blinkLED(void)
{
	IfxPort_togglePin(LED_D107);
}

void blinkLED2(void)
{
	IfxPort_togglePin(LED_D108);
}

void blinkLED3(void)
{
	IfxPort_togglePin(LED_D109);
}

void blinkLED4(void)
{
	IfxPort_togglePin(LED_D110);
}

void blinkLED5(void)
{
	IfxPort_togglePin(LED_D111);
}

void turn_on_led4(void)
{
	IfxPort_setPinLow(LED_D110);
}

void turn_on_led5(void)
{
	IfxPort_setPinLow(LED_D111);
}


void turn_off_led4(void)
{
	IfxPort_setPinHigh(LED_D110);
}

void turn_off_led5(void)
{
	IfxPort_setPinHigh(LED_D111);
}
