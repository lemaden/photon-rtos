/*
 * PhotonRTOS础光实时操作系统 -- 字节序
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_BYTEORDER_H
#define __ASM_BYTEORDER_H


#include <photon/byteorder/little_endian.h>

#endif	/* __ASM_BYTEORDER_H */
