/*
 * PhotonRTOS础光实时操作系统 -- 屏障相关
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_BARRIER_H
#define __ASM_BARRIER_H
#include <IfxCpu_IntrinsicsGnuc.h>
#ifndef __ASSEMBLY__

/* The "volatile" is due to gcc bugs */
#define barrier() __asm__ __volatile__("" : : : "memory")


#define mb()		Ifx__dsync();
#define rmb()		Ifx__dsync();
#define wmb()		Ifx__dsync();

#define smp_mb()	Ifx__dsync();

#endif	/* __ASSEMBLY__ */

#endif	/* __ASM_BARRIER_H */
