/*
 * PhotonRTOS础光实时操作系统 -- 中断头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_IRQFLAGS_H
#define __ASM_IRQFLAGS_H

#include "IfxAsclin_Asc.h"
#include "IfxCpu_Irq.h"
#include "Ifx_Console.h"
#define ARCH_FUN_arch_local_irq_save_DEFINED
static inline uintptr_t arch_local_irq_save(void)
{
    uintptr_t flag;

    flag = (uintptr_t)IfxCpu_disableInterrupts();

    return !flag;
}

#define ARCH_FUN_arch_local_irq_enable_DEFINED
static inline void arch_local_irq_enable(void)
{
    IfxCpu_enableInterrupts();
}

#define ARCH_FUN_arch_local_irq_disable_DEFINED
static inline void arch_local_irq_disable(void)
{
    IfxCpu_disableInterrupts();
}

#define ARCH_FUN_arch_local_irq_restore_DEFINED
static inline void arch_local_irq_restore(uintptr_t flags)
{
    IfxCpu_restoreInterrupts((boolean)!flags);
}

#define ARCH_FUN_arch_irqs_disabled_flags_DEFINED
static inline uintptr_t arch_irqs_disabled_flags(uintptr_t flags)
{
    if (flags == 1) {
    	return 0;
    } else {
    	return 1;
    }
}

/*
 * 保存恢复中断寄存器
 */
#define ARCH_FUN_arch_local_save_flags_DEFINED
static inline uintptr_t arch_local_save_flags(void)
{
    uintptr_t flags;

    flags = (uintptr_t)IfxCpu_areInterruptsEnabled();
    return flags;
}

/**
 * TODO：二类中断相关处理：保存中断状态，启用/关闭，可能需要修改
*/
#define ARCH_FUN_arch_local_isr_save_DEFINED
extern bool cat_2_interrupt;
static inline uintptr_t arch_local_isr_save(void)
{
	uintptr_t flags;
	flags = (uintptr_t)cat_2_interrupt;
	cat_2_interrupt = false;
	return flags;
}

#define ARCH_FUN_arch_local_isr_restore_DEFINED
static inline void arch_local_isr_restore(uintptr_t flags)
{
	cat_2_interrupt = (bool)flags;
}


#endif


