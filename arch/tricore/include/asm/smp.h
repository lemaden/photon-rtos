/*
 * PhotonRTOS础光实时操作系统 -- 多核
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_SMP_H

#define __ASM_SMP_H

#ifndef __ASSEMBLY__
#include <photon/process.h>
#include <photon/smp.h>
#include <photon/cpumask.h>
#include <IfxCpu.h>

struct cpumask;
enum ipi_cmd_type;

#define ARCH_FUN_smp_processor_id_DEFINED
#define smp_processor_id() (current_proc_info()->cpu)

static int arch_get_core_id(void)
{
	return IfxCpu_getCoreIndex();
}

extern void arch_start_slave(struct slave_cpu_data *slave_cpu_data);

#define ARCH_FUN_arch_raise_ipi_DEFINED
void arch_raise_ipi(const struct cpumask *mask, enum ipi_cmd_type cmd_type);
void smp_set_ioc_isr(void (*fn)(void));
#endif /* __ASSEMBLY__ */

#endif /* __ASM_SMP_H */
