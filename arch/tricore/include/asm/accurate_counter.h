/*
 * PhotonRTOS础光实时操作系统 -- 精确计数头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <baoyou.xie@aliyun.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_ACCURATE_COUNTER_H
#define __ASM_ACCURATE_COUNTER_H
#include <asm/types.h>

struct accurate_counter;

#define ARCH_FUN_arch_accurate_add_DEFINED
#define ARCH_FUN_arch_accurate_sub_DEFINED
#define ARCH_FUN_arch_accurate_cmpxchg_DEFINED
#define ARCH_FUN_arch_accurate_xchg_DEFINED
intptr_t arch_accurate_add(intptr_t i, struct accurate_counter *v);
intptr_t arch_accurate_sub(intptr_t i, struct accurate_counter *v);
intptr_t arch_accurate_cmpxchg(struct accurate_counter *ptr, intptr_t old, intptr_t new);
uintptr_t arch_accurate_xchg(uintptr_t x, volatile void *ptr, int32_t size);
#endif /* __ASM_ACCURATE_COUNTER_H */
