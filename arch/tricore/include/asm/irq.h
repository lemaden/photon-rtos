/*
 * PhotonRTOS础光实时操作系统 -- 中断头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_ARM_IRQ_H
#define __ASM_ARM_IRQ_H

#define __exception	__attribute__((section(".text")))
#define __exception_irq_entry	__exception

/**
 * NR_IRQS 支持的最大中断数
 *
 */
#define NR_IRQS 192U

#define ISR_PRIORITY_STM_CORE0 40

#define ISR_PRIORITY_STM_CORE1 41

#define ISR_PRIORITY_CORE_INTER_CORE0 42

#define ISR_PRIORITY_CORE_INTER_CORE1 43

#define ISR_PRIORITY_CORE_SHUTDOWN_CORE0 44

#define ISR_PRIORITY_CORE_SHUTDOWN_CORE1 45

#define ISR_PRIORITY_CORE_IOC_CORE0 46

#define ISR_PRIORITY_CORE_IOC_CORE1 47

/**
 * set_chip_irq_handle
 *
 * @param handle_irq
 */
extern void set_chip_irq_handle(void (*handle_irq)(void *regs));
#define ARCH_FUN_set_chip_irq_handle_DEFINED
extern void arch_irq_init(void);
extern void arch_task_init(void *stack, void* func, void *task);

extern void gic_secondary_init(void);
extern void irq_controller_init(void);

#endif
