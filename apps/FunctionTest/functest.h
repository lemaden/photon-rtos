/*
 * 础光实时操作系统PhotonRTOS -- 功能测试框架
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */


#ifndef _FUNCTEST_H
#define _FUNCTEST_H

#include "sequence_test.h"

#define FUNCTEST_START()									\
	printk(											\
"[=================================开始功能测试=================================]\n")

#define FUNCTEST_END()										\
	printk(											\
"[=================================结束功能测试=================================]\n")

#define TEST_SUIT(name)										\
	void test_suit_##name(void);								\
	void test_suit_##name(void)

#define TEST(name)										\
	void test_case_##name(void);								\
	void test_case_##name(void)

#define UTEST_SUITE(func)						\
	do {								\
		printk("[----------] 开始测试用例：{ %s }\n", #func);	\
		test_suit_##func();					\
		printk("[----------] 结束测试用例：{ %s }\n", #func);	\
	} while (0)

#define UTEST_CASE(func)						\
	do {								\
		printk("[ RUN      ] 执行单元：[ %s ]\n", #func);	\
		test_case_##func();					\
	} while (0)

#define FUNCTEST_SUITE(func)									\
	do {											\
		printk(										\
"[--------------------------------------] 开始测试功能：{ %s }\n", #func);			\
		test_suit_##func();								\
		printk(										\
"[--------------------------------------] 结束测试功能：{ %s }\n", #func);			\
	} while (0)

#define FUNCTEST_CASE(func)									\
	do {											\
		printk("[-----------------------------] 执行测试用例：[ %s ]\n", #func);	\
		test_case_##func();								\
	} while (0)

#define LOGI(fmt, ...)										\
	printk(" * " fmt, ##__VA_ARGS__)

#define SEQUENCE_RECORD		sequence_record

#define SEQUENCE_RECORD_VALUE	sequence_record_value

#define ASSERT_SEQUENCE(RIGHT)									\
	do {											\
		if (assert_sequence_records(RIGHT)) {						\
			printk("[XXXXXXXXXXXXXXXXXXXXXXXXXXXXX] 测试失败\n");			\
		} else {									\
			printk("[-----------------------------] 测试通过\n");			\
		}										\
	} while (0)


#endif
