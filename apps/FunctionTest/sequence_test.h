/*
 * 础光实时操作系统PhotonRTOS -- 运行时序 测试框架
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef _SEQUENCE_TEST_H
#define _SEQUENCE_TEST_H

#include <Os_internel.h>

#define MAX_SEQUENCE 100
#define SEQUENCE_END (uint8_t)0XFFU

/* 记录一个指定值到序列中,line为空则不输出任何内容 */
FUNC(void, OS_CODE) sequence_record_value(
	VAR(uint8, AUTOMATIC) value,
	P2VAR(const int8_t, AUTOMATIC, OS_APPL_DATA) line, ...
);

/* 记录任务号到序列中,line为空则不输出任何内容 */
FUNC(void, OS_CODE) sequence_record(
	P2VAR(const int8_t, AUTOMATIC, OS_APPL_DATA) line, ...
);

/**
 * 验证记录的序列与输入序列是否一致,并清空序列
 * 一致返回 0, 不一致返回 1
 */
FUNC(uint8, OS_CODE) assert_sequence_records(
	P2VAR(uint8, AUTOMATIC, OS_APPL_CONST) right_sequ
);

#endif /* _SEQUENCE_TEST_H */

