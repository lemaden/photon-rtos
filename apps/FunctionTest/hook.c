/*
 * 础光实时操作系统PhotonRTOS -- 用户hook
 * 用于指定应用特定的信息，
 * 例如不同模式下的任务、需要自动激活的任务
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <Os_internel.h>

void StartupHook(void)
{
	printk("StartupHook: 启动系统...... 核心:%d\n", GetCoreID());
}

void PostTaskHook(void);
void PostTaskHook(void)
{
}

void ShutdownHook(StatusType error);
void ShutdownHook(StatusType error)
{
	printk("关闭系统......\n");
}

void ErrorHook(StatusType code);
void ErrorHook(StatusType code)
{
	OSServiceIdType service_id;

	service_id = OSErrorGetServiceId();
	switch (service_id) {
	case OSServiceId_ActivateTask:
		printk("ErrorHook: 错误状态码为 %d, 服务ID是 ActivateTask: %d！\n",
			code, OSServiceId_ActivateTask);
		break;
	default:
		printk("ErrorHook: 错误状态码为 %d, 未识别的服务: %d！\n",
			code, service_id);
		break;
	};
}
