/*
 * 础光实时操作系统PhotonRTOS -- 事件唤醒功能测试代码
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#include "../functest.h"

#define EVENT_1		0x1
#define IdTestTask4	4
#define IdTestTask5	5
#define IdTestTask6	6

VAR(uint8, AUTOMATIC) event_sequence_right[MAX_SEQUENCE] = {
	IdTestTask4, IdTestTask4, IdTestTask5, IdTestTask5,
	IdTestTask6, IdTestTask6, IdTestTask5, IdTestTask5,
	IdTestTask6, IdTestTask6, IdTestTask5, IdTestTask4,
	IdTestTask4, SEQUENCE_END,
};

TEST(event)
{
	LOGI("事件设置测试开始\n");
	LOGI("优先级: task4 < task5 < task6\n");
	ActivateTask(IdTestTask4);
	ActivateTask(IdTestTask5);
	ASSERT_SEQUENCE(event_sequence_right);
}


TASK(TestTask4, app0)
{
	SEQUENCE_RECORD("\t|\t\t\t task start");

	SEQUENCE_RECORD("\t|\t\t\t wait event");
	WaitEvent(EVENT_1);
	SEQUENCE_RECORD("\t|\t\t\t get event");

	SEQUENCE_RECORD("\t|\t\t\t task end");
	TerminateTask();
}

TASK(TestTask5, app0)
{
	SEQUENCE_RECORD("\t\t|\t\t task start");

	SEQUENCE_RECORD("\t\t|\t\t activate task6");
	ActivateTask(IdTestTask6);

	SEQUENCE_RECORD("\t\t|\t\t set event at task4");
	SetEvent(IdTestTask4, EVENT_1);

	SEQUENCE_RECORD("\t\t|\t\t set event at task6");
	SetEvent(IdTestTask6, EVENT_1);

	SEQUENCE_RECORD("\t\t|\t\t task end");
	TerminateTask();
}

TASK(TestTask6, app0)
{
	SEQUENCE_RECORD("\t\t\t|\t task start");

	SEQUENCE_RECORD("\t\t\t|\t wait event");
	WaitEvent(EVENT_1);
	SEQUENCE_RECORD("\t\t\t|\t get event");

	SEQUENCE_RECORD("\t\t\t|\t task end");
	TerminateTask();
}


TEST_SUIT(event)
{
	FUNCTEST_CASE(event);
}

