/*
 * 础光实时操作系统PhotonRTOS -- 功能测试代码
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <Os_internel.h>

/**
 * =========================================配置任务属性=========================================
 */
DeclareTask(FunctionTest, AUTOSTART(1), PRIORITY(1), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(5000000), APP_ID(0), SchedPolicy(SCHED_TIMESLICE));

DeclareTask(TestTask1, AUTOSTART(0), PRIORITY(2), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask2, AUTOSTART(0), PRIORITY(3), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask3, AUTOSTART(0), PRIORITY(4), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask4, AUTOSTART(0), PRIORITY(2), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask5, AUTOSTART(0), PRIORITY(3), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask6, AUTOSTART(0), PRIORITY(4), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask7, AUTOSTART(0), PRIORITY(2), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

DeclareTask(TestTask8, AUTOSTART(0), PRIORITY(2), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));

#if defined(AUTOSAR_SYNCHRONIZATION_SUPPORT)
DeclareTask(TestTask9, AUTOSTART(0), PRIORITY(2), TASK_FLAG(TF_EXTENDED),
	APP_MODE(OSDEFAULTAPPMODE), CORE_ID(0), TASK_TIME_FRAME(0),
	ISR_TIME_FRAME(0), ISR_TIME_BUDGET(10000), TASK_TIME_BUDGET(1000000), APP_ID(0), SchedPolicy(SCHED_FIFO));
#endif /* defined(AUTOSAR_SYNCHRONIZATION_SUPPORT) */


/**
 * 所有预定义任务列表，这里只定义任务属性
 * 由生成工具产生
 */
struct osek_task_attr *osek_task_attrs[OS_NR_TASK] = {
	[0] = &OS_TASK_ATTR_FunctionTest,
	[1] = &OS_TASK_ATTR_TestTask1,
	[2] = &OS_TASK_ATTR_TestTask2,
	[3] = &OS_TASK_ATTR_TestTask3,
	[4] = &OS_TASK_ATTR_TestTask4,
	[5] = &OS_TASK_ATTR_TestTask5,
	[6] = &OS_TASK_ATTR_TestTask6,
	[7] = &OS_TASK_ATTR_TestTask7,
	[8] = &OS_TASK_ATTR_TestTask8,
#if defined(AUTOSAR_SYNCHRONIZATION_SUPPORT)
	[9] = &OS_TASK_ATTR_TestTask9,
#endif /* defined(AUTOSAR_SYNCHRONIZATION_SUPPORT) */

};
#ifdef CONFIG_AUTOSAR_MEMORY_PROTECTION
struct app_address_range app_address_range[AUTOSAR_NR_APPLICATION] = {
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE}
		},
		{
				data_range : {__app1_data_start, __app1_data_end, READABLE | WRITABLE}
		},
};

struct task_address_range task_address_range[OS_NR_TASK] = {
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{

				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},
		{
				data_range : {__app0_data_start, __app0_data_end, READABLE | WRITABLE},
				code_range : {__app0_code_start, __app0_code_end, EXECUTABLE},
		},

};
#endif

/**
 * =========================================配置alarm属性=========================================
 */
struct osek_alarm_attr *osek_alarm_attrs[OS_NR_ALARM] = {

};

#if defined(AUTOSAR_OS_APPLICATION)
/**
 * =========================================配置os-app属性=========================================
 */
struct os_application_attr autosar_os_app_attr[AUTOSAR_NR_APPLICATION] = {

};
#endif /* defined(AUTOSAR_OS_APPLICATION) */

