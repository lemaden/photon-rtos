/*
 * PhotonRTOS础光实时操作系统 -- 系统日志测试代码
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"
#include <Os.h>

TEST(SystemLog) 
{
	StatusType t;

	#define TEST_SIZE 512
    int8_t test[TEST_SIZE] = {'\0'};
	/**
	 * 日志等级高于默认等级的无法进入缓冲区
	 * （数值上小于）
	 */
	pr_err("系统日志测试: 高\n");
	t = GetSysLog(test, TEST_SIZE, true);
	ASSERT_EQ(t, E_OK);
	printk("%s", test);
	
	/**
	 * 日志等级低于等于默认等级的无法进入缓冲区
	 * （数值上大于等于）
	 */
	pr_debug("系统日志测试：低\n");
	t = GetSysLog(test, TEST_SIZE, true);
	ASSERT_EQ(t, E_OS_VALUE);
	
	#undef TEST_SIZE
}

TEST_SUIT(SysLog) 
{
	UTEST_CASE(SystemLog);
}
