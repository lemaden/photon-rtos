/*
 * PhotonRTOS础光实时操作系统 -- multicore测试代码
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"

extern VAR(CoreStatueType, OS_VAR_INIT) autosar_core_status[MAX_CPUS];

TEST(GetNumberOfActivatedCores)
{
	VAR(uint32, AUTOMATIC) num_activated_cores = 0;

	/**
	 * []函数GetNumberOfActivatedCores应可从 TASK 和 ISR cat 2 中调用。否则行为未指定。
	 */


	/**
	 * [SWS_Os_00673] ⌈ GetNumberOfActivatedCores的返回值应小于或等于“ OsNumberOfCores ”的配置值。 ⌋ ( SRS_Os_80001 )
	 */
	num_activated_cores = GetNumberOfActivatedCores();
	ASSERT_TRUE(num_activated_cores <= OsNumberOfCores);

}


TEST(GetCoreID)
{
	VAR(CoreIdType, AUTOMATIC) core_ID = OsNumberOfCores;

	/**
	 * [SWS_Os_00675] ⌈ GetCoreID 函数应返回调用该函数的核心的唯一逻辑 CoreID。物理核心到逻辑 CoreID 的映射是特定于实现的。 ⌋ ( SRS_Os_80001 )
	 */
	core_ID = GetCoreID();
	ASSERT_TRUE(core_ID < OsNumberOfCores);

}


TEST(StartCore)
{
	VAR(StatusType, AUTOMATIC) st;

#if defined(EXTENDED_STATUS)
	/**
	 * []E_OS_ID: Core ID is invalid
	 */
	StartCore(OsNumberOfCores, &st);
	ASSERT_EQ(st, E_OS_ID);

	/**
	 * [SWS_Os_00678] ⌈在 StartOS() 之后调用StartCore函数应返回E_OS_ACCESS，并且不应启动核心。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
	StartCore(0, &st);
	ASSERT_NE(st, E_OK);

	/**
	 * [SWS_Os_00679] ⌈如果参数CoreIDs引用了一个已经由函数StartCore启动的核心，则忽略相关的核心并返回 E_OS_STATE。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
	StartCore(0, &st);
	ASSERT_EQ(st, E_OS_STATE);

	StartCore(1, &st);
	ASSERT_EQ(st, E_OS_STATE);

	/**
	 * [SWS_Os_00680] ⌈如果参数CoreID引用了一个已经由函数StartNonAutosarCore启动的核心，则忽略相关核心并返回 E_OS_STATE。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
	autosar_core_status[1] = NONE_AUTOSAR_CORE;
	StartCore(1, &st);
	autosar_core_status[1] = AUTOSAR_CORE;
	ASSERT_EQ(st, E_OS_STATE);
#endif

	/**
	 * [SWS_Os_00677] ⌈ StartCore 函数将启动一个在 AUTOSAR OS 控制下运行的核心。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */


	/**
	 * [SWS_Os_00681] ⌈如果在 StartCore()期间发生错误，则不会调用ErrorHook()； ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
}


TEST(StartNonAutosarCore)
{
	VAR(StatusType, AUTOMATIC) st;

#if defined(EXTENDED_STATUS)
	/**
	 * [SWS_Os_00684] ⌈如果参数CoreID引用了一个已经由函数StartNonAutosarCore启动的核心，则将“Status”设置为 E_OS_STATE。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
	autosar_core_status[1] = NONE_AUTOSAR_CORE;
	StartNonAutosarCore(1, &st);
	autosar_core_status[1] = AUTOSAR_CORE;
	ASSERT_EQ(st, E_OS_STATE);

	/**
	 * [SWS_Os_00685] ⌈如果参数CoreID引用了一个未知的核心函数，则StartNonAutosarCore无效并将“状态”设置为 E_OS_ID。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */
	StartNonAutosarCore(OsNumberOfCores, &st);
	ASSERT_EQ(st, E_OS_ID);
#endif

	/**
	 * [SWS_Os_00683] ⌈ StartNonAutosarCore函数将启动一个不受AUTOSAR OS控制的核心。 ⌋ ( SRS_Os_80006 , SRS_Os_80026, SRS_Os_80027 )
	 */


}


TEST(ShutdownAllCores)
{

	/**
	 * [SWS_Os_00714] ⌈应由 API 函数ShutdownAllCores 触发同步关机。 ⌋ ( SRS_Os_80007 )
	 */


	/**
	 * [SWS_Os_00715] ⌈ ShutdownAllCores不得返回。 ⌋ ( SRS_Os_80007 )
	 */


	/**
	 * [SWS_Os_00716] ⌈如果ShutdownAllCores 是从非可信代码调用的，则该调用将被忽略。 ⌋ ( SRS_Os_80007 )
	 */
	ShutdownAllCores(E_OK);

}



TEST_SUIT(Multicore)
{
	UTEST_CASE(GetNumberOfActivatedCores);
	UTEST_CASE(GetCoreID);
#if (CONFIG_MAX_CPUS > 1)
	UTEST_CASE(StartCore);
	UTEST_CASE(StartNonAutosarCore);
#endif
#ifdef CONFIG_AUTOSAR_SHUTDOWNALLCORES_TEST
	UTEST_CASE(ShutdownAllCores);
#endif
}

