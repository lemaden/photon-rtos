/*
 * PhotonRTOS础光实时操作系统 -- Event测试
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#include "../utest.h"
#include <Os.h>

TEST(GetEvent)
{
	VAR(TaskType, AUTOMATIC) tid;
	VAR(StatusType, AUTOMATIC) st;
	VAR(EventMaskType, AUTOMATIC) evmask;

	/* 获取当前任务的事件mask，预期数值为0 */
	st = GetTaskID(&tid);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0);
}

TEST(SetEvent)
{
	VAR(TaskType, AUTOMATIC) tid;
	VAR(StatusType, AUTOMATIC) st;
	VAR(EventMaskType, AUTOMATIC) evmask;

	/* 设置当前任务的事件mask */
	st = GetTaskID(&tid);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0);

	st = SetEvent(tid, 0x0f);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0x0f);

	st = SetEvent(tid, 0xf0);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0xff);

	/* 清除所有事件 */
	st = ClearEvent(0xff);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0);
}

TEST(ClearEvent)
{
	VAR(TaskType, AUTOMATIC) tid;
	VAR(StatusType, AUTOMATIC) st;
	VAR(EventMaskType, AUTOMATIC) evmask;

	st = GetTaskID(&tid);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0);

	st = SetEvent(tid, 0xff);
	ASSERT_EQ(st, E_OK);

	st = ClearEvent(0x0f);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0xf0);

	st = ClearEvent(0xf0);
	ASSERT_EQ(st, E_OK);

	st = GetEvent(tid, &evmask);
	ASSERT_EQ(st, E_OK);
	ASSERT_EQ(evmask, 0);
}

TEST(WaitEvent)
{
	VAR(TaskType, AUTOMATIC) tid;
	VAR(StatusType, AUTOMATIC) st;

	st = GetTaskID(&tid);
	ASSERT_EQ(st, E_OK);

	st = SetEvent(tid, 0x0f);
	ASSERT_EQ(st, E_OK);

	st = WaitEvent(0x0f);
	ASSERT_EQ(st, E_OK);

	st = SetEvent(tid, 0xf0);
	ASSERT_EQ(st, E_OK);

	st = WaitEvent(0xf0);
	ASSERT_EQ(st, E_OK);

	st = ClearEvent(0xff);
	ASSERT_EQ(st, E_OK);
}

#if defined(AUTOSAR_OS_APPLICATION)
TEST(SetEventAsyn)
{

	/**
	 * [SWS_Os_00819] ⌈ SetEventAsyn()的可用性：在支持 OS 应用程序的系统中可用。 ⌋ (SRS_Os_80015)
	 * 注意：如果在事件设置期间发生错误并且调用者已经离开（例如调用者 OS-Application 已经终止，或者调用者核心正在关闭或 ...）对错误钩子的调用将被丢弃并且不进行报告。
	 */
	SetEventAsyn(2, 0x4);

}
#endif /* defined(AUTOSAR_OS_APPLICATION) */


TEST_SUIT(Event)
{
	UTEST_CASE(GetEvent);
	UTEST_CASE(SetEvent);
	UTEST_CASE(ClearEvent);
	UTEST_CASE(WaitEvent);
#if defined(AUTOSAR_OS_APPLICATION)
	UTEST_CASE(SetEventAsyn);
#endif /* defined(AUTOSAR_OS_APPLICATION) */
}
