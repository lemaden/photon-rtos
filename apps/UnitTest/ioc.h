/*
 * 础光实时操作系统PhotonRTOS -- IOC通讯(自动生成部分)
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Rui Yang <yangrui@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_IOC_H
#define OS_IOC_H

#include <autosar/ioc_internal.h>

#define IOCTESTSTART 2000
#define IOCTESTEND 2001

extern VAR(struct InnerQueueGroup, AUTOMATIC) group_queue[4];
extern VAR(struct InnerQueueGroup, AUTOMATIC) nogroup_queue[4];
extern VAR(struct InnerQueueGroup, AUTOMATIC) nogroup_noqueue[4];
extern VAR(struct InnerQueueGroup, AUTOMATIC) group_noqueue[4];

FUNC(void, OS_CODE) auto_gen_ioc_init(void);

FUNC(Std_ReturnType, OS_CODE) IocSend_SecondDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC)  length1);

FUNC(Std_ReturnType, OS_CODE) IocReceive_SecondDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC)  length1);

FUNC(Std_ReturnType, OS_CODE) IocSend_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2);

FUNC(Std_ReturnType, OS_CODE) IocReceive_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2);


FUNC(Std_ReturnType, OS_CODE) IocWrite_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocRead_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocRead_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocWrite_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocSend_QUEUE_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocRec_QUEUE_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocWrite_CORE1_TO_CORE0(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

FUNC(Std_ReturnType, OS_CODE) IocRead_CORE1_TO_CORE0(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3);

#endif