/*
 * 础光实时操作系统PhotonRTOS -- 用户hook
 * 用于指定应用特定的信息，
 * 例如不同模式下的任务、需要自动激活的任务
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "utest.h"

void StartupHook(void)
{
	LOGI("StartupHook: 启动系统...... 核心:%d\n", GetCoreID());
}

void PostTaskHook(void);
void PostTaskHook(void)
{
}

void ShutdownHook(StatusType error);
void ShutdownHook(StatusType error)
{
	LOGI("关闭系统......\n");
}

void ErrorHook(StatusType code);
void ErrorHook(StatusType code)
{
	OSServiceIdType service_id;

	service_id = OSErrorGetServiceId();
	switch (service_id) {
	case OSServiceId_ActivateTask:
		LOGI("ErrorHook: 错误状态码为 %d, 服务ID是 ActivateTask: %d！",
			code, OSServiceId_ActivateTask);
		break;
	default:
		LOGI("ErrorHook: 错误状态码为 %d, 未识别的服务: %d！",
			code, service_id);
		break;
	};
}

void StartupHookApp0(void);
void StartupHookApp0(void)
{
	LOGI("App0: StartupHook");
}

void ShutdownHookApp0(void);
void ShutdownHookApp0(void)
{
	LOGI("App0: ShutdownHook");
}

void ErrorHookApp0(StatusType err);
void ErrorHookApp0(StatusType err)
{
	LOGI("App0: ErrorHook: status %d", err);
}

#if AUTOSAR_NR_APPLICATION > 1
void StartupHookApp1(void);
void StartupHookApp1(void)
{
	LOGI("App1: StartupHook");
}

void ShutdownHookApp1(void);
void ShutdownHookApp1(void)
{
	LOGI("App1: ShutdownHook");
}

void ErrorHookApp1(StatusType err);
void ErrorHookApp1(StatusType err)
{
	LOGI("App1: ErrorHook: status %d", err);
}
#endif
