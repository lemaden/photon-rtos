/*
 * 础光实时操作系统PhotonRTOS -- IOC通讯
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Rui Yang <yangrui@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "ioc.h"

FUNC(Std_ReturnType, OS_CODE) IocSend_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 0;
    data.IocId = 0;
    data.ElementNum = 2;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    return IocSend(&data);
}


FUNC(Std_ReturnType, OS_CODE) IocReceive_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 0;
    data.IocId = 0;
    data.ElementNum = 2;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    return IocReceive(&data);
}


FUNC(Std_ReturnType, OS_CODE) IocSend_SecondDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC)  length1)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 0;
    data.IocId = 0;
    data.ElementNum = 1;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    return IocSend(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocReceive_SecondDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC)  length1)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 0;
    data.IocId = 0;
    data.ElementNum = 1;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    return IocReceive(&data);
}

/**
 * for 1:1 with gruop whitout queue
*/
FUNC(Std_ReturnType, OS_CODE) IocWrite_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 0;
    data.IocId = 0;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocWrite(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocRead_FirstDemo(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 0;
    data.IocId = 0;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocRead(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocWrite_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 0;
    data.IocId = 1;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocWrite(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocRead_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 1;
    data.IocId = 1;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocRead(&data);
}


FUNC(Std_ReturnType, OS_CODE) IocSend_QUEUE_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 0;
    data.IocId = 1;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocSend(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocRec_QUEUE_CORE0_TO_CORE1(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 1;
    data.IocId = 1;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocReceive(&data);
}


FUNC(Std_ReturnType, OS_CODE) IocWrite_CORE1_TO_CORE0(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.SenderId = 1;
    data.IocId = 2;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocWrite(&data);
}

FUNC(Std_ReturnType, OS_CODE) IocRead_CORE1_TO_CORE0(
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) a, 
    VAR(int, AUTOMATIC) length1, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) b, 
    VAR(int, AUTOMATIC) length2, 
    P2VAR(int, AUTOMATIC, OS_APPL_DATA) c, 
    VAR(int, AUTOMATIC) length3)
{
    VAR(struct IocData, AUTOMATIC) data;
    data.ReceiverId = 0;
    data.IocId = 2;
    data.ElementNum = 3;
    data.Element[0].PData = (uint8_t *)a;
    data.Element[0].DataSize = length1;
    data.Element[1].PData = (uint8_t *)b;
    data.Element[1].DataSize = length2;
    data.Element[2].PData = (uint8_t *)c;
    data.Element[2].DataSize = length3;
    return IocRead(&data);
}