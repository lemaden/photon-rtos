/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR内部接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_INTERNAL_H
#define OS_AUTOSAR_INTERNAL_H

#include <autosar/error.h>
#include <autosar/multicore_internal.h>
#include <autosar/schedule_table_internal.h>
#include <autosar/counter_internal.h>
#include <autosar/os_application_internal.h>
#include <autosar/object_internal.h>
#include <autosar/common.h>
#include <autosar/memory_internal.h>
#include <autosar/isr_internal.h>
#include <autosar/peripheral_internal.h>
#include <autosar/spinlock_internal.h>
#include <autosar/task_internal.h>
#include <autosar/alarm_internal.h>
#include <autosar/event_internal.h>
#include <autosar/resource_internal.h>
#include <autosar/hooks_internal.h>
#include <autosar/system_internal.h>
#include <autosar/calllevel_internal.h>
#include <autosar/ioc_internal.h>
#ifdef CONFIG_IOC
#include "ioc.h"
#endif

#define AUTOSAR_NR_SPIN_LOCK CONFIG_AUTOSAR_SPINLOCK_COUNT

#define INVALID_SPINLOCK 0

#define RESTART 0
#define NO_RESTART 1

#define ACCESS 0

#define NO_ACCESS 1

enum {
		/**
		 * 此状态下，该app的Object可以被其它app访问，为开机后的默认状态。
		 */
		APPLICATION_ACCESSIBLE,
		/**
		 * 该app的Object不可以被其它app访问，但是可以通过AllowAccess()转换成ACCESSIBLE状态。
		 */
		APPLICATION_RESTARTING,
		/**
		 * 该app的Object永远都不可以被其他app访问了。
		 */
		APPLICATION_TERMINATED
};

enum {
		/**
		 * counter类型
		 */
		TYPE_COUNTER,
		/**
		 * alarm类型
		 */
		TYPE_ALARM,
		/**
		 * 调度表类型
		 */
		TYPE_SCHEDULE_TABLE,
		/**
		 * 任务类型
		 */
		TYPE_TASK,
		/**
		 * ISR类型
		 */
		TYPE_ISR,
		/**
		 * 类型的数量
		 */
		MAX_TYPE_NUM,
};

#define OSMEMORY_IS_UNACCESSABLE 0

#define OSMEMORY_IS_WRITEABLE 1 << 1

#define OSMEMORY_IS_READABLE  1 << 2

enum {
		PRO_IGNORE,
		PRO_TERMINATETASKISR,
		PRO_TERMINATEAPPL,
		PRO_TERMINATEAPPL_RESTART,
		PRO_SHUTDOWN
};

typedef VAR(uint8_t, TYPEDEF) ProtectionReturnType;

struct autosar_area_peripheral {
		VAR(AreaIdType, AUTOMATIC)
		AreaId;
		P2VAR(uint32, AUTOMATIC, OS_APPL_DATA)
		address;
		VAR(uint32, TYPEDEF)
		size;
};
typedef VAR(struct autosar_area_peripheral, TYPEDEF) AUTOSAR_AREA_PERIPHERAL;
#if defined(CONFIG_AUTOSAR_MAX_PERIPHERAL)
		extern VAR(AUTOSAR_AREA_PERIPHERAL, AUTOMATIC)
				area_peripheral[CONFIG_AUTOSAR_MAX_PERIPHERAL];
#endif

struct autosar_smp_lock {
		VAR(struct smp_lock, TYPEDEF)
		smp_lock;
		VAR(ApplicationType, TYPEDEF)
		accessable_application_id[AUTOSAR_NR_APPLICATION];
		VAR(CoreIdType, TYPEDEF)
		owner_core_id;
		VAR(ApplicationType, TYPEDEF)
		owner_app_id;
};

FUNC(CoreIdType, OS_CODE)
		autosar_get_core_id(void);

extern VAR(struct autosar_smp_lock, AUTOMATIC)
		autosar_spin_locks[AUTOSAR_NR_SPIN_LOCK];

extern VAR(SpinlockIdType, AUTOMATIC)
		autosar_spin_locks_order[AUTOSAR_NR_SPIN_LOCK];

extern VAR(struct smp_lock, TYPEDEF) activate_task_async_lock;

extern VAR(struct smp_lock, TYPEDEF) set_event_async_lock;

extern VAR(bool, AUTOMATIC) time_protect_table[MAX_CPUS];

#define time_protect_disable (time_protect_table[smp_processor_id()])

FUNC(void, OS_CODE)
autosar_init_smp_spin_lock(void);

FUNC(ApplicationType, OS_CODE)
autosar_get_application_id(void);

FUNC(ApplicationType, OS_CODE)
autosar_get_current_application_id(void);

FUNC(void, OS_CODE)
autosar_call_trusted_function(
		VAR(TrustedFunctionIndexType, AUTOMATIC) FunctionIndex,
		VAR(TrustedFunctionParameterRefType, AUTOMATIC) FunctionParams);

FUNC(AccessType, OS_CODE)
autosar_check_isr_memory_access(
		VAR(ISRType, AUTOMATIC) ISRID,
		VAR(MemoryStartAddressType, AUTOMATIC) Address,
		VAR(MemorySizeType, AUTOMATIC) Size);

FUNC(AccessType, OS_CODE)
autosar_check_task_memory_access(
		VAR(TaskType, AUTOMATIC) TaskID,
		VAR(MemoryStartAddressType, AUTOMATIC) Address,
		VAR(MemorySizeType, AUTOMATIC) Size);

FUNC(ObjectAccessType, OS_CODE)
autosar_check_object_access(
		VAR(ApplicationType, AUTOMATIC) ApplID,
		VAR(ObjectTypeType, AUTOMATIC) ObjectType,
		P2VAR(void, AUTOMATIC, OS_APPL_DATA) object);

FUNC(ApplicationType, OS_CODE)
autosar_check_object_ownership(
		VAR(ObjectTypeType, AUTOMATIC) ObjectType,
		P2VAR(void, AUTOMATIC, OS_APPL_DATA) object);

FUNC(StatusType, OS_CODE)
autosar_terminate_application(
		VAR(ApplicationType, AUTOMATIC) Application,
		VAR(RestartType, AUTOMATIC) RestartOption);

FUNC(StatusType, OS_CODE)
autosar_allow_access(void);

FUNC(StatusType, OS_CODE)
autosar_get_application_state(
		VAR(ApplicationType, AUTOMATIC) Application,
		VAR(ApplicationStateRefType, AUTOMATIC) Value);

FUNC(void, OS_CODE) autosar_preinit(void);

FUNC(StatusType, OS_CODE)
autosar_activate_task_async(TaskType TaskID, ApplicationType CallerAppId);

FUNC(StatusType, OS_CODE)
autosar_set_event_async(TaskType TaskID, EventMaskType Mask, ApplicationType caller_app_id);

FUNC(void, OS_CODE) autosar_protect_hook(
	VAR(StatusType, AUTOMATIC) Fatalerror);

#endif /* OS_AUTOSAR_INTERNAL_H */
