/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR多核类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_MULTICORE_TYPES_H
#define OS_MULTICORE_TYPES_H

#include <autosar/common_types.h>

#if !defined(OsNumberOfCores)
#define OsNumberOfCores CONFIG_MAX_CPUS
#endif

#if !defined(OsNumberOfNonAutosarCores)
#define OsNumberOfNonAutosarCores CONFIG_NON_AUTOSAR_CORES
#endif

#define OS_CORE_ID_VALID_MASK ((1U << OsNumberOfCores) - 1)

typedef enum {
	OS_CORE_ID_0 = 0,
	OS_CORE_ID_MASTER = OS_CORE_ID_0,
#if (OS_CORE_ID_VALID_MASK & 0x02U)
	OS_CORE_ID_1 = 1,
#endif /* OS_CORE_ID_VALID_MASK & 0x02U */
#if (OS_CORE_ID_VALID_MASK & 0x04U)
	OS_CORE_ID_2 = 2,
#endif /* OS_CORE_ID_VALID_MASK & 0x04U */
#if (OS_CORE_ID_VALID_MASK & 0x08U)
	OS_CORE_ID_3 = 3,
#endif /* OS_CORE_ID_VALID_MASK & 0x08U */
#if (OS_CORE_ID_VALID_MASK & 0x10U)
	OS_CORE_ID_4 = 4,
#endif /* OS_CORE_ID_VALID_MASK & 0x10U */
#if (OS_CORE_ID_VALID_MASK & 0x20U)
	OS_CORE_ID_5 = 5,
#endif /* OS_CORE_ID_VALID_MASK & 0x20U */
#if (OS_CORE_ID_VALID_MASK & 0x40U)
	OS_CORE_ID_6 = 6,
#endif /* OS_CORE_ID_VALID_MASK & 0x40U */
#if (OS_CORE_ID_VALID_MASK & 0x80U)
	OS_CORE_ID_7 = 7,
#endif /* OS_CORE_ID_VALID_MASK & 0x80U */
	OS_CORE_ID_NR,
} autosar_core_id_type;

typedef VAR(autosar_core_id_type, TYPEDEF) CoreIdType;
typedef VAR(uint8, TYPEDEF) IdleModeType;

#endif /* OS_MULTICORE_TYPES_H */
