/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR内存接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_MEMORY_H
#define OS_MEMORY_H

#include <autosar/memory_types.h>
#include <autosar/isr_types.h>
#include <autosar/task_types.h>

#if defined(AUTOSAR_MEMORY_PROTECTION)
/**
 * 如果	ISR 引用 <ISRID> 在调用CheckISRMemoryAccess()时有效,
 * CheckISRMemoryAccess()将返回 ISR 对指定内存区域的访问权限。
 * 语法：
 *      AccessType CheckISRMemoryAccess (
 *              ISRType ISRID,
 *              MemoryStartAddressType Address,
 *              MemorySizeType Size )
 * 服务 ID [hex] :
 *      0x03
 * 同步/异步 :
 *      同步(Synchronous)
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      ISRID  ISR 参考
 *      Address 内存区域的开始。
 *      Size    内存区域大小
 * 参数（输出）：
 *      无
 * 返回值：
 *      AccessType 包含对内存区域的访问权限的值。
 * 描述：
 *      此服务检查内存区域是否可写/读/执行访问，如果内存区域是堆栈空
 * 间的一部分，还会返回信息。
 * 引用：
 *      AUTOSAR OS 8.4.5 CheckISRMemoryAccess [SWS_Os_00512]
 */
FUNC(AccessType, OS_CODE) CheckISRMemoryAccess(
	VAR(ISRType, AUTOMATIC) ISRID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size);

/**
 * 如果在CheckTaskMemoryAccess()调用中的任务引用 <TaskID>有效，
 * CheckTaskMemoryAccess()将返回任务对指定内存区域的访问权限。
 * 如果访问权限（例如“读取”）对于在CheckTaskMemoryAccess()调用中指定的整
 * 个内存区域无效，
 * 则CheckTaskMemoryAccess ()将不会产生与此权限相关的访问权限。
 * 语法：
 *      AccessType CheckTaskMemoryAccess(
 *               TaskType TaskID,
 *               MemoryStartAddressType Address,
 *               MemorySizeType Size)
 * 服务 ID [hex] :
 *      0x04
 * 同步/异步 :
 *      同步(Synchronous)
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      TaskID  任务参考
 *      Address 内存区域的开始。
 *      Size    内存区域大小
 * 参数（输出）：
 *      无
 * 返回值：
 *      AccessType 包含对内存区域的访问权限的值。
 * 描述：
 *      此服务检查内存区域是否可写/读/执行访问，如果内存区域是堆栈空
 * 间的一部分，还会返回信息。
 * 引用：
 *      AUTOSAR OS 8.4.6 CheckTaskMemoryAccess [SWS_Os_00513]
 */
FUNC(AccessType, OS_CODE) CheckTaskMemoryAccess(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size);
#endif /* defined(AUTOSAR_MEMORY_PROTECTION) */

#endif /* OS_MEMORY_H */
