/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR闹钟配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ALARM_CONFIG_H
#define OS_ALARM_CONFIG_H

#define OS_NR_ALARM CONFIG_OSEK_ALARMS_COUNT

/**
 * ALARMCALLBACK宏定义，定义一个警报（定时器）回调函数
 *        name：任务名称
 */
#define ALARMCALLBACK(Name)					\
extern void OS_CALLBACK_##Name(void);			\
void OS_CALLBACK_##Name(void)
#define DeclareAlarm(Name, CounterID, AppMode)			\
	extern void OS_CALLBACK_##Name(void);		\
	struct osek_alarm_attr OS_ALARM_ATTR_##Name =	\
	{							\
		.callback =OS_CALLBACK_##Name,	\
		.counterID = CounterID,				\
		.app_mode = AppMode,				\
	}

#endif
