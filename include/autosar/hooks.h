/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR钩子接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_HOOKS_H
#define OS_HOOKS_H

#include <autosar/hooks_types.h>
#include <autosar/system_types.h>

/**
 * 留给应用的钩子，在切换出应用时调用
 */
extern FUNC(void, OS_CODE) PostTaskHook(void);
/**
 * 留给应用的钩子，在异常关机时调用
 */
extern FUNC(void, OS_CODE) ShutdownHook(VAR(StatusType,AUTOMATIC) error);
/**
 * 留给应用的钩子，在启动系统时调用
 */
extern FUNC(void, OS_CODE) StartupHook(void);

extern FUNC(void, OS_CODE) ErrorHook(VAR(StatusType,AUTOMATIC) code);

extern VAR(OSServiceIdType, AUTOMATIC) osek_hook_service_id;

LOCAL_INLINE FUNC(OSServiceIdType, OS_CODE) OSErrorGetServiceId(void)
{
	return osek_hook_service_id;
}

#endif /* OS_HOOKS_H */
