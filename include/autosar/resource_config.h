/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR资源配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_RESOURCE_CONFIG_H
#define OS_RESOURCE_CONFIG_H

#define OS_NR_RESOURCE CONFIG_OSEK_RESOURCES_COUNT

#endif