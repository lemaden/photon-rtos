/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR系统内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SYSTEM_INTERNAL_H
#define OS_SYSTEM_INTERNAL_H

#include <autosar/system.h>
#include <autosar/calllevel_internal.h>

typedef enum {
	OSEK_SYSTEM_INITIALIZED,	/* OSEK系统初始化之前 */
	OSEK_SYSTEM_STARTING,		/* OSEK系统调用了StartOS启动，
					 * 在StartupHook之前 */
	OSEK_SYSTEM_STARTED,		/* OSEK系统在StartupHook之后 */
	OSEK_SYSTEM_SHUTDOWN,		/* OSEK系统在调用ShutdownOS之后 */
} osek_system_state;

extern VAR(volatile osek_system_state, AUTOMATIC) osek_system;

#endif /* OS_SYSTEM_INTERNAL_H */
