/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR错误处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ERROR_H
#define OS_ERROR_H

#include <autosar/common_types.h>
#include <autosar/compiler.h>
/**
 * 错误码：
 *     E_OK：无错误
 *     E_OS_ACCESS：无权限
 *     E_OS_CALLEVEL：调用级别，例如不允许在中断中调用
 *     E_OS_ID：错误的ID
 *     E_OS_LIMIT：超过限制数量
 *     E_OS_NOFUNC：警报（定时器）未在使用中
 *     E_OS_RESOURCE：调用任务正在占据资源，因此不能等待事件
 *     E_OS_STATE：状态不正确，因此调用异常，可能的原因：
 *        由于引用任务处于挂起状态，事件不能被设置
 *        警报已经在使用中
 *     E_OS_VALUE：设置的值不  正确
 */
typedef enum {
	E_OK = 0,
	E_OS_ACCESS,
	E_OS_CALLEVEL,
	E_OS_ID,
	E_OS_LIMIT,
	E_OS_NOFUNC,
	E_OS_RESOURCE,
	E_OS_STATE,
	E_OS_VALUE,
	/* AUTOSAR CP 错误号 */
	E_OS_SERVICEID,
	E_OS_ILLEGAL_ADDRESS,
	E_OS_MISSINGEND,
	E_OS_DISABLEDINT,
	E_OS_STACKFAULT,
	E_OS_PARAM_POINTER,
	E_OS_PROTECTION_MEMORY,
	E_OS_PROTECTION_TIME,
	E_OS_PROTECTION_ARRIVAL,
	E_OS_PROTECTION_LOCKED,
	E_OS_PROTECTION_EXCEPTION,
	E_OS_SPINLOCK,
	E_OS_INTERFERENCE_DEADLOCK,
	E_OS_NESTING_DEADLOCK,
	E_OS_CORE,
} StatusType;

FUNC(char *, OS_CODE) Status2String(VAR(StatusType, AUTOMATIC) status);

#endif /* OS_ERROR_H */
