/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR自旋锁接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SPINLOCK_H
#define OS_SPINLOCK_H

#include <autosar/spinlock_types.h>


/**
 * 服务名字
 *		GetSpinlock
 * 语法
 *		StatusType GetSpinlock ( SpinlockIdType SpinlockId )
 * 服务 ID [hex]
 *		0x19
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *		SpinlockId		该值是指应锁定的自旋锁实例。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *		    StatusType
 *          E_OK - 处于标准和扩展状态：无错误
 *          E_OS_ID - 处于扩展状态：SpinlockId 无效
 *          E_OS_INTERFERENCE_DEADLOCK - 处于扩展状态：一个任务
 *	      当锁已经被同一个核心上的一个 TASK 占用时，尝试占用自旋锁
 * 。这会导致死锁。
 *          E_OS_NESTING_DEADLOCK - 处于扩展状态：一个 TASK 试图占用自旋锁，
 *	      而同一核心上的一个 TASK 以可能导致死锁的方式持有不同的自
 * 旋锁。
 *          E_OS_ACCESS - 处于扩展状态：无法访问自旋锁。
 * 描述
 *	     GetSpinlock 试图占用一个自旋锁变量。如果函数返回，则要么成功
 * 获取了锁，要么发生了错误。
 *              自旋锁机制是一种主动轮询机制。该函数不会导致取消调度。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.27 GetSpinlock [SWS_Os_00686]
 */
FUNC(StatusType, OS_CODE) GetSpinlock(VAR(SpinlockIdType, AUTOMATIC) SpinlockId);

/**
 * Service Name
 *		ReleaseSpinlock
 * 语法
 *		StatusType ReleaseSpinlock (
 *              SpinlockIdType SpinlockId
 *      )
 * 服务 ID [hex]
 *		0x1a
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *      SpinlockId		该值是指应锁定的自旋锁实例。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		None
 * 返回值
 *		StatusType
 *              E_OK - 标准和扩展状态：无错误
 *              E_OS_ID - 处于扩展状态：SpinlockId 无效。
 *              E_OS_STATE - 处于扩展状态：自旋锁未被 TASK 占用
 *              E_OS_ACCESS - 处于扩展状态：无法访问自旋锁。
 *	      E_OS_NOFUNC - 处于扩展状态：尝试释放自旋锁而之前必须释放另一
 * 个自旋锁（或资源）。
 * 描述
 *		ReleaseSpinlock 释放一个之前被占用的自旋锁变量。
 *      在终止一个 TASK 之前，所有被 Get Spinlock() 占用的自旋锁变量都应该被
 * 释放。
 *       在调用 WaitEvent 之前，应释放所有自旋锁。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.28 ReleaseSpinlock [SWS_Os_00695]
 */
FUNC(StatusType, OS_CODE) ReleaseSpinlock(
	VAR(SpinlockIdType, AUTOMATIC) SpinlockId);


/**
 * Service Name
 *		TryToGetSpinlock
 * 语法
 *		StatusType TryToGetSpinlock (
 *              SpinlockIdType SpinlockId,
 *              TryToGetSpinlockType* Success
 *      )
 * 服务 ID [hex]
 *		0x1b
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *		SpinlockId		该值是指应锁定的自旋锁实例。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *		Success		返回锁是否被占用
 * 返回值
 *		StatusType
 *		E_OK - 标准和扩展状态：无错误
 *              E_OS_ID - 处于扩展状态：SpinlockId 无效。
 *              E_OS_INTERFERENCE_DEADLOCK - 处于扩展状态：一个任务
 *	      当锁已经被同一个核心上的一个 TASK 占用时，尝试占用自旋锁
 * 。这会导致死锁。
 *              E_OS_NESTING_DEADLOCK - 处于扩展状态：TASK 尝试占用一个自旋锁，
 *                      同时以可能导致死锁的方式持有不同的自旋锁。
 *              E_OS_ACCESS - 处于扩展状态：无法访问自旋锁。
 * 描述
 *		TryToGetSpinlock 具有与 GetSpinlock 相同的功能，不同之处在于，
 *      如果自旋锁已被不同核心上的 TASK 占用，则该函数设置 OUT 参数“
 * Success”并返回 E_OK。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.29 TryToGetSpinlock [SWS_Os_00703]
 */
FUNC(StatusType, OS_CODE) TryToGetSpinlock(
	VAR(SpinlockIdType, AUTOMATIC) SpinlockId,
	P2VAR(TryToGetSpinlockType, AUTOMATIC, OS_APPL_DATA) Success);

#endif /* OS_SPINLOCK_H */
