/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR调度表配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_SCHEDULE_TABLE_CONFIG_H
#define OS_AUTOSAR_SCHEDULE_TABLE_CONFIG_H

#if defined(AUTOSAR_SYNCHRONIZATION_SUPPORT)
#define DeclareScheduleTable(ScheduleTableID, Duration, Cycle, CounterID, Sync, Precision)	\
	autosar_schedule_tables[ScheduleTableID].sync = Sync;					\
	list_init(&(autosar_schedule_tables[ScheduleTableID].expriypts));			\
	autosar_schedule_tables[ScheduleTableID].status = SCHEDULETABLE_STOPPED;		\
	autosar_schedule_tables[ScheduleTableID].next_pt = NULL_PTR;				\
	autosar_schedule_tables[ScheduleTableID].next_schedtblID = NO_NEXT_SCHEDULE_TABLE;	\
	autosar_schedule_tables[ScheduleTableID].deviation = 0;					\
	smp_lock_init(&(autosar_schedule_tables[ScheduleTableID].lock));			\
	autosar_schedule_tables[ScheduleTableID].duration = Duration;				\
	autosar_schedule_tables[ScheduleTableID].cycle = Cycle;					\
	bind_schedule_table_to_counter(ScheduleTableID, CounterID);				\
	autosar_schedule_tables[ScheduleTableID].precision = Precision;				\

#define DeclareExpiryPoint(ExpiryPointID, Time, ScheduleTableID, Shorten, Lengthen)		\
	autosar_expiry_points[ExpiryPointID].time = Time;					\
	__add_expiry_point_to_schedule_table(							\
		&(autosar_expiry_points[ExpiryPointID]), ScheduleTableID);			\
	autosar_expiry_points[ExpiryPointID].max_shorten = Shorten;				\
	autosar_expiry_points[ExpiryPointID].max_lengthen = Lengthen;				\
	list_init(&autosar_expiry_points[ExpiryPointID].tasks);					\
	list_init(&autosar_expiry_points[ExpiryPointID].events);				\

#else /* defined(AUTOSAR_SYNCHRONIZATION_SUPPORT) */
#define DeclareScheduleTable(ScheduleTableID, Duration, Cycle, CounterID, Sync, Precision)	\
	list_init(&(autosar_schedule_tables[ScheduleTableID].expriypts));			\
	autosar_schedule_tables[ScheduleTableID].status = SCHEDULETABLE_STOPPED;		\
	autosar_schedule_tables[ScheduleTableID].next_pt = NULL_PTR;				\
	autosar_schedule_tables[ScheduleTableID].next_schedtblID = NO_NEXT_SCHEDULE_TABLE;	\
	smp_lock_init(&(autosar_schedule_tables[ScheduleTableID].lock));			\
	autosar_schedule_tables[ScheduleTableID].duration = Duration;				\
	autosar_schedule_tables[ScheduleTableID].cycle = Cycle;					\
	bind_schedule_table_to_counter(ScheduleTableID, CounterID);				\

#define DeclareExpiryPoint(ExpiryPointID, Time, ScheduleTableID, Shorten, Lengthen)		\
	autosar_expiry_points[ExpiryPointID].time = Time;					\
	__add_expiry_point_to_schedule_table(							\
		&(autosar_expiry_points[ExpiryPointID]), ScheduleTableID);			\
	list_init(&autosar_expiry_points[ExpiryPointID].tasks);					\
	list_init(&autosar_expiry_points[ExpiryPointID].events);				\

#endif /* defined(AUTOSAR_SYNCHRONIZATION_SUPPORT) */

#define DeclareExpiryPointTask(ExpiryPointTaskID, TaskID, ExpiryPointID)		\
	autosar_expiry_point_tasks[ExpiryPointTaskID].taskID = TaskID;			\
	list_insert_behind(&(autosar_expiry_point_tasks[ExpiryPointTaskID].list),	\
		&(autosar_expiry_points[ExpiryPointID].tasks));				\


#define DeclareExpiryPointEvent(ExpiryPointTaskID, TaskID, Event, ExpiryPointID)	\
	autosar_expiry_point_events[ExpiryPointTaskID].taskID = TaskID;			\
	autosar_expiry_point_events[ExpiryPointTaskID].event = Event;			\
	list_insert_behind(&(autosar_expiry_point_events[ExpiryPointTaskID].list),	\
		&(autosar_expiry_points[ExpiryPointID].events));			\

#endif