/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR编译器抽象
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_COMPILER_H
#define OS_AUTOSAR_COMPILER_H

#include <autosar/compiler_cfg.h>

/**
 * 默认编译器抽象
 */
#if (!defined(AUTOMATIC))
#define AUTOMATIC
#endif /* !AUTOMATIC */

#if (!defined(TYPEDEF))
#define TYPEDEF
#endif /* !TYPEDEF */

#if (!defined(NULL_PTR))
#define NULL_PTR  ((void *)0)
#endif /* !NULL_PTR */

#if (!defined(INLINE))
#define INLINE       OS_INLINE
#endif /* !INLINE */

#if (!defined(LOCAL_INLINE))
#define LOCAL_INLINE  OS_STATIC_INLINE
#endif /* !LOCAL_INLINE */

#if (!defined(VAR))
#define VAR(vartype, memclass) vartype memclass
#endif /* !VAR */

#if (!defined(CONST))
#define CONST(consttype, memclass) consttype const memclass
#endif /* !CONST */

#if (!defined(P2VAR))
# define P2VAR(ptrtype, memclass, ptrclass) ptrtype memclass * ptrclass
#endif /* !P2VAR */

#if (!defined(P2CONST))
#define P2CONST(ptrtype, memclass, ptrclass) ptrtype const memclass * ptrclass
#endif /* !P2CONST */

#if (!defined(CONSTP2VAR))
#define CONSTP2VAR(ptrtype, memclass, ptrclass)				\
	ptrtype memclass * const ptrclass
#endif /* !CONSTP2VAR */

#if (!defined(CONSTP2CONST))
#define CONSTP2CONST(ptrtype, memclass, ptrclass)			\
	ptrtype const memclass *  const ptrclass
#endif /* !CONSTP2CONST */

#if (!defined(P2FUNC))
#define P2FUNC(rettype, ptrclass, fctname) rettype(*ptrclass fctname)
#endif /* !P2FUNC */

#if (!defined(FUNC))
#define FUNC(rettype, memclass) rettype memclass
#endif /* !FUNC */

#if (!defined(FUNC_P2VAR))
#define FUNC_P2VAR(rettype, ptrclass, memclass) rettype * ptrclass memclass
#endif /* !FUNC_P2VAR */

#if (!defined(FUNC_P2CONST))
#define FUNC_P2CONST(rettype, ptrclass, memclass)			\
	rettype const *ptrclass memclass
#endif /* !FUNC_P2CONST */

/**
 * 默认操作系统内存类
 */
#if (!defined(OS_CODE))
#define OS_CODE                     AUTOMATIC
#endif /* !OS_CODE */

#if (!defined(OS_CONST))
#define OS_CONST                    AUTOMATIC
#endif /* !OS_CONST */

#if (!defined(OS_APPL_CODE))
#define OS_APPL_CODE                AUTOMATIC
#endif /* !OS_APPL_CODE */

#if (!defined(OS_APPL_DATA))
#define OS_APPL_DATA                AUTOMATIC
#endif /* !OS_APPL_DATA */

#if (!defined(OS_APPL_CONST))
#define OS_APPL_CONST               AUTOMATIC
#endif /* !OS_APPL_CONST */

#if (!defined(REGSPACE))
#define REGSPACE                    TYPEDEF
#endif /* !REGSPACE */

#if (!defined(OS_VAR_NO_INIT))
#define OS_VAR_NO_INIT              AUTOMATIC
#endif /* !OS_VAR_NO_INIT */

#if (!defined(OS_VAR_CLEARED))
#define OS_VAR_CLEARED              AUTOMATIC
#endif /* !OS_VAR_ CLEARED */

#if (!defined(OS_VAR_POWER_ON_CLEARED))
#define OS_VAR_POWER_ON_CLEARED     AUTOMATIC
#endif /* !OS_VAR_POWER_ON_CLEARED */

#if (!defined(OS_VAR_INIT))
#define OS_VAR_INIT                 AUTOMATIC
#endif /* !OS_VAR_INIT */

#if (!defined(OS_VAR_POWER_ON_INIT))
#define OS_VAR_POWER_ON_INIT        AUTOMATIC
#endif /* !OS_VAR_POWER_ON_CLEARED */

#endif /* OS_AUTOSAR_COMPILER_H */
