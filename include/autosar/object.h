/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR对象接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_OBJECT_H
#define OS_OBJECT_H

#include <autosar/object_types.h>

#if defined(AUTOSAR_OS_APPLICATION)
/**
 * 如果调用CheckObjectAccess()中的 OS-Application <ApplID>
 * 有权访问查询的对象，CheckObjectAccess()应返回ACCESS 。
 * 如果调用CheckObjectAccess()中的 OS-Application <ApplID>
 * 无权访问查询的对象，CheckObjectAccess()应返回NO_ACCESS 。
 * 语法：
 *      ObjectAccessType CheckObjectAccess (ApplicationType ApplID,
 *              ObjectTypeType ObjectType, void ... )
 * 服务 ID [hex] :
 *      0x05
 * 同步/异步 :
 *      同步(Synchronous)
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      ApplID   操作系统-应用程序标识符
 *      ObjectType  以下参数的类型
 *      ...    检查对象
 * 参数（输出）：
 *      无
 * 返回值：
 *      ObjectAccessType 如果 ApplID 有权访问该对象，ACCESS，否则，NO_ACCESS。
 * 描述：
 *      此服务确定是否允许由 ApplID 提供的 OS-应用程序
 *      在 API 调用中使用任务、资源、计数器、闹钟或调度表的 ID。
 * 引用：
 *      AUTOSAR OS 8.4.7 CheckObjectAccess [SWS_Os_00256]
 */
FUNC(ObjectAccessType, OS_CODE) CheckObjectAccess(
	VAR(ApplicationType, AUTOMATIC) ApplID,
	VAR(ObjectTypeType, AUTOMATIC) ObjectType,
	P2VAR(void, AUTOMATIC, OS_APPL_DATA) object);

/**
 * 如果在CheckObjectOwnership()调用中指定的对象 ObjectType存在，
 * CheckObjectOwnership()将返回该对象所属的 OS-Application 的标识符。
 * 如果在CheckObjectOwnership()调用中指定的对象 ObjectType 无效或类型的参数（
 * “...”）
 * 无效或对象不属于任何 OS-Application， CheckObjectOwnership()应返回
 * INVALID_OSAPPLICATION .
 * 语法：
 *      ApplicationType CheckObjectOwnership (ObjectTypeType ObjectType, void ...)
 * 服务 ID [hex] :
 *      0x06
 * 同步/异步 :
 *      同步(Synchronous)
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      ObjectType  以下参数的类型
 *      ...    检查对象
 * 参数（输出）：
 *      无
 * 返回值：
 *      <OS-Application>：对象ObjectType 属于的OS-Application，
 *      或者，如果对象不存在，则为 INVALID_OSAPPLICATION类型
 * 描述：
 *      此服务确定给定任务、ISR、计数器、闹钟或调度表属于哪个操作系统
 * 应用程序
 * 引用：
 *      AUTOSAR OS 8.4.8 CheckObjectOwnership [SWS_Os_00017]
 */
FUNC(ApplicationType, OS_CODE) CheckObjectOwnership(
	VAR(ObjectTypeType, AUTOMATIC) ObjectType,
	P2VAR(void, AUTOMATIC, OS_APPL_DATA) object);
#endif /* defined(AUTOSAR_OS_APPLICATION) */

#endif /* OS_OBJECT_H */
