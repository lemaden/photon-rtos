/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR资源内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_RESOURCE_INTERNAL_H
#define OS_RESOURCE_INTERNAL_H

#include <autosar/resource.h>
#include <autosar/calllevel_internal.h>

struct osek_resource {
	/**
	 * 资源优先级
	 * 当任务获得资源以后，将提升其调度优先级
	 */
	VAR(int32_t, TYPEDEF) prio;
};
extern VAR(struct osek_resource,AUTOMATIC) osek_resources[];

#endif /* OS_RESOURCE_INTERNAL_H */
