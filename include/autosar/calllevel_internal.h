/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR调用等级
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Rui Yang <yangrui@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_CALLLEVEL_INTERNAL_H
#define OS_CALLLEVEL_INTERNAL_H

#include <autosar/common_types.h>
#include <autosar/os_application_types.h>

typedef enum {
	_ENV_TASK,
	_ENV_CAT1_ISR,
	_ENV_CAT2_ISR,
	_ENV_ERROR_HOOK,
	_ENV_PRETASK_HOOK,
	_ENV_POSTTASK_HOOK,
	_ENV_STARTUP_HOOK,
	_ENV_SHUTDOWN_HOOK,
	_ENV_ALARM_CALLBACK,
	_ENV_PROTECTION_HOOK,
	_ENV_MASK,
}os_run_context;

#define ENV_TASK				(1U << _ENV_TASK)
#define ENV_CAT1_ISR			(1U << _ENV_CAT1_ISR)
#define ENV_CAT2_ISR			(1U << _ENV_CAT2_ISR)
#define ENV_ERROR_HOOK			(1U << _ENV_ERROR_HOOK)
#define ENV_PRETASK_HOOK		(1U << _ENV_PRETASK_HOOK)
#define ENV_POSTTASK_HOOK		(1U << _ENV_POSTTASK_HOOK)
#define ENV_STARTUP_HOOK		(1U << _ENV_STARTUP_HOOK)
#define ENV_SHUTDOWN_HOOK		(1U << _ENV_SHUTDOWN_HOOK)
#define ENV_SHUTDOWN_HOOK		(1U << _ENV_SHUTDOWN_HOOK)
#define ENV_ALARM_CALLBACK		(1U << _ENV_ALARM_CALLBACK)
#define ENV_PROTECTION_HOOK		(1U << _ENV_PROTECTION_HOOK)
#define ENV_OS_RUN_CONTEXT_MAX	ENV_PROTECTION_HOOK
#define ENV_MASK				((1u << _ENV_MASK) - 1)

FUNC(ApplicationType, OS_CODE)
refresh_os_run_context(
	VAR(ApplicationType, AUTOMATIC) context);

FUNC(void, OS_CODE)
restore_os_run_context(
	VAR(ApplicationType, AUTOMATIC) context);

FUNC(StatusType, OS_CODE)
os_run_context_seted(
	VAR(ApplicationType, AUTOMATIC) context);

FUNC(void, OS_CODE)
os_run_context_init(void);

#define CONTEXT_MARK_START(VAL)				{ FUNC(ApplicationType, OS_CODE) old_context = refresh_os_run_context(VAL);
#define CONTEXT_MARK_END					restore_os_run_context(old_context); }
#define CONTEXT_CHECK(VAL)					if(os_run_context_seted(VAL) != E_OK) return E_OS_CALLEVEL;
#define CONTEXT_CHECK_NO_RETURN_VAL(VAL)	if(os_run_context_seted(VAL) != E_OK) return;
#define CONTEXT_CHECK_IN_BOOT(VAL)			if(os_run_context_seted(VAL) == E_OK) return;

#endif /* OS_CALLLEVEL_INTERNAL_H */
