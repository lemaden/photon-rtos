/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR自旋锁类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SPINLOCK_TYPES_H
#define OS_SPINLOCK_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint16, TYPEDEF) SpinlockIdType;

/**
 * TryToGetSpinlockType 指示自旋锁是否已被占用。
 */
typedef enum {
	TRYTOGETSPINLOCK_SUCCESS = 0x0,
	TRYTOGETSPINLOCK_NOSUCCESS
} TryToGetSpinlockType;

#endif /* OS_SPINLOCK_TYPES_H */
