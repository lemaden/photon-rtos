/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR自旋锁内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SPINLOCK_INTERNAL_H
#define OS_SPINLOCK_INTERNAL_H

#include <autosar/spinlock.h>
#include <autosar/calllevel_internal.h>

#endif /* OS_SPINLOCK_INTERNAL_H */
