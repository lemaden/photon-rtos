/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR中断接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ISR_H
#define OS_ISR_H

#include <autosar/isr_types.h>

/**
 * GetISRID()如果从 2 类 ISR（或在 2 类 ISR 内调用的 Hook 例程）调用，
 * 获取 ISRID() 应返回当前执行的 ISR 的标识符。
 * 如果它的调用者不是 2 类 ISR（或在 2 类 ISR 中调用的 Hook 例程），
 * GetISRID()应返回INVALID_ISR 。
 * 语法：
 *      ISRType GetISRID ( void )
 * 服务 ID [hex] :
 *      0x01
 * 同步/异步 :
 *      同步（Synchronous）
 * 可重入性:
 *      可重入
 * 参数（输入）：
 *      无
 * 参数（输出）：
 *      无
 * 返回值：
 *      ISRType  <Identifier of running ISR> or INVALID_ISR
 * 描述：
 *      该服务返回当前正在执行的 ISR 的标识符。
 * 引用：
 *      AUTOSAR OS 8.4.3 GetISRID [SWS_Os_00511]
 */
FUNC(ISRType, OS_CODE) GetISRID(void);

/**
 * 服务名字
 *      EnableInterruptSource
 * 语法
 *		StatusType EnableInterruptSource (   ISRType ISRID,   boolean ClearPending )
 * 服务 ID [hex]
 *		0x31
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *      ISRID    2 类 ISR 的 ID。
 *      ClearPending    定义挂起标志是否应被清除 (TRUE) 或不 (FALSE)。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *  None
 * 返回值
 *      StatusType
 *		    E_OK 无错误。
 *      E_OS_ID ISRID 不是有效的第 2 类 ISR 标识符（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用应用程序不是 ISRID（服务保护）传入的 ISR 的所有者
 * 描述
 *	 通过修改中断控制器寄存器启用中断源。此外，它可能会清除中断
 * 挂起标志
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.35 EnableInterruptSource [SWS_Os_91020]
 */
FUNC(StatusType, OS_CODE) EnableInterruptSource(
	VAR(ISRType, AUTOMATIC) ISRID,
	VAR(boolean, AUTOMATIC) ClearPending);

/**
 * 服务名字
 *      DisableInterruptSource
 * 语法
 *		StatusType DisableInterruptSource ( ISRType ISRID )
 * 服务 ID [hex]
 *		0x30
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *      ISRID    2 类 ISR 的 ID。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *  None
 * 返回值
 *      StatusType
 *		    E_OK 无错误。
 *      E_OS_ID ISRID 不是有效的第 2 类 ISR 标识符（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用应用程序不是 ISRID（服务保护）传入的 ISR 的所有者
 * 描述
 *		通过修改中断控制器寄存器来禁用中断源。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.36 disableInterruptSource [SWS_Os_91019]
 */
FUNC(StatusType, OS_CODE) DisableInterruptSource(
	VAR(ISRType, AUTOMATIC) ISRID);

/**
 * 服务名字
 *      ClearPendingInterrupt
 * 语法
 *		StatusType ClearPendingInterrupt ( ISRType ISRID )
 * 服务 ID [hex]
 *		0x32
 * 同步/异步
 *		同步（Synchronous）
 * 可重入性
 *		可重入
 * 参数 (in)
 *      ISRID    2 类 ISR 的 ID。
 * 参数 (inout)
 *		None
 * 参数 (out)
 *  None
 * 返回值
 *      StatusType
 *		    E_OK 无错误。
 *      E_OS_ID ISRID 不是有效的第 2 类 ISR 标识符（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用应用程序不是 ISRID（服务保护）传入的 ISR 的所有者
 * 描述
 *		通过修改中断控制器寄存器清除中断挂起标志。
 * 引用
 *		Os.h
 *      AUTOSAR OS 8.4.37 ClearPendingInterrupt [SWS_Os_91021]
 */
FUNC(StatusType, OS_CODE) ClearPendingInterrupt(
	VAR(ISRType, AUTOMATIC) ISRID);

/**
 * 禁止所有硬件所支持的中断。
 * 在此之前的状态被保存以用于EnableAllInterrupts调用。
 * 语法：
 *     void DisableAllInterrupts ( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在一类或者二类ISR、任务级中调用，
 *     但是不能从钩子例程中调用。
 *     该服务目的是开始代码临界区。
 *     该服务应该通过调用EnableAllInterrupts来结束。
 *     在该临界区中，不允许API服务调用。
 *     实现应当适配这个服务到目标硬件，并提供最小负载。
 *     通常，此服务可以让中央处理单元禁止识别中断。
 *     注意：该服务不支持嵌套。
 *     如果临界区中的嵌套是必要的，
 *     使用库SuspendOSInterrupts/ResumeOSInterrupts
 *     或者SuspendAllInterrupt/ResumeAllInterrupts
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *         无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) DisableAllInterrupts(void);

/**
 * 恢复由DisableAllInterrupts保存的状态。
 * 语法：
 *     void EnableAllInterrupts ( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在一类或者二类ISR、任务级中调用，
 *     但是不能从钩子例程中调用。
 *     该服务是DisableAllInterrupts的相对服务，
 *     DisableAllInterrupts服务必须先调用，
 *     其目的是实现代码临界区。
 *     在该临界区中，不允许API服务调用。
 *     实现应当适配这个服务到目标硬件，并提供最小负载。
 *     通常，此服务可以让中央处理单元处理中断。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) EnableAllInterrupts(void);

/**
 * 恢复中断状态，
 * 这些状态由SuspendAllInterrupts服务所保存。
 * 语法：
 *     void ResumeAllInterrupts( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在一类或者二类ISR、
 *     警报回调、任务级中调用，
 *     但是不能从所有钩子例程中调用。
 *     该服务是SuspendAllInterrupts服务的相对服务。
 *     在这之前调用此服务，
 *     其目的是结束临界区代码。
 *     除了 SuspendAllInterrupts/ResumeAllInterrupts 对和
 *     SuspendOSInterrupts/ResumeOSInterrupts 对之外，
 *     不允许在此临界区中调用任何 API 服务。
 *     实现应当适配这个服务到目标硬件，并提供最小负载。
 *     SuspendAllInterrupts/ResumeAllInterrupts可以嵌套。
 *     在嵌套调用SuspendAllInterrupts/ResumeAllInterrupts情况下，
 *     第一个调用SuspendAllInterrupts保存的中断状态会被
 *     最后一个调用ResumeAllInterrupts服务来恢复。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) ResumeAllInterrupts(void);

/**
 * 恢复由SuspendOSInterrupts服务保存的中断状态。
 * 语法：
 *     void ResumeOSInterrupts ( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在一类或者二类ISR、任务级中调用，
 *     但是不能从钩子例程中调用。
 *     该服务是SuspendOSInterrupts服务的相对服务，
 *     SuspendOSInterrupts服务必须在之前调用，
 *     其目的是结束代码临界区。
 *     除了 SuspendAllInterrupts/ResumeAllInterrupts 对和
 *     SuspendOSInterrupts/ResumeOSInterrupts 对之外，
 *     不允许在此临界区中调用任何 API 服务。
 *     SuspendOSInterrupts/ResumeOSInterrupts可以嵌套。
 *     在嵌套调用SuspendOSInterrupts
 *     和ResumeOSInterrupts的情况下，
 *     由第一个SuspendOSInterrupts 保存的
 *     中断状态将被最后一个ResumeOSInterrupts恢复。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) ResumeOSInterrupts(void);

/**
 * 保存所有中断状态，并禁止硬件支持的所有中断。
 * 语法：
 *     void SuspendAllInterrupts( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在一类或者二类ISR、
 *        警报回调、任务级中调用
 *     但是不能从所有钩子例程中调用。
 *       该服务的目的是保存所有类型的
 *     中断相关的临界区代码。
 *     应当通过调用ResumeAllInterrupts服务来结束。
 *     除了 SuspendAllInterrupts/ResumeAllInterrupts 对和
 *       SuspendOSInterrupts/ResumeOSInterrupts 对之外，
 *     不允许在此临界区中调用任何 API 服务。
 *     实现应当适配这个服务到目标硬件，并提供最小负载。
 * 状态：
 * 标准：
 *     无
 * 扩展：
 *     无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) SuspendAllInterrupts(void);

/**
 * 保存二类中断的状态，并禁止这些中断。
 * 语法：
 *     void SuspendOSInterrupts( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     该服务可以在ISR、任务级中调用，
 *     但是不能从钩子例程中调用。
 *     该服务的目的是保护代码临界区。
 *     该临界区应当通过调用ResumeOSInterrupts服务来结束。
 *     除了SuspendAllInterrupts/ResumeAllInterrupts和
 *        SuspendOSInterrupts/ResumeOSInterrupts外，
 *     在临界区内不允许调用其他API服务。
 *     实现应当适配本服务到目标硬件，并提供最小负载。
 *     它仅仅试图禁止所有二类中断。
 * 不过，这不是有效的禁止更多中断的方式。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) SuspendOSInterrupts(void);

#endif /* OS_ISR_H */
