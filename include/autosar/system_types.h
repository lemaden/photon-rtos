/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR系统类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SYSTEM_TYPES_H
#define OS_SYSTEM_TYPES_H

#include <autosar/common_types.h>

typedef P2VAR(void, TYPEDEF, OS_APPL_DATA) MemoryStartAddressType;
typedef VAR(uint32, TYPEDEF) MemorySizeType;

typedef enum {
	OSDEFAULTAPPMODE = 0,
	DONOTCARE,
	APPMODECOUNT,
} AppModeType;

typedef enum {
	OSServiceId_ActivateTask = 0,
	OSServiceId_CancelAlarm,
	OSServiceId_ChainTask,
	OSServiceId_ClearEvent,
	OSServiceId_DisableAllInterrupts,
	OSServiceId_EnableAllInterrupts,
	OSServiceId_GetActiveApplicationMode,
	OSServiceId_GetAlarm,
	OSServiceId_GetAlarmBase,
	OSServiceId_GetEvent,
	OSServiceId_GetResource,
	OSServiceId_GetTaskID,
	OSServiceId_GetTaskState,
	OSServiceId_ReleaseResource,
	OSServiceId_ResumeAllInterrupts,
	OSServiceId_ResumeOSInterrupts,
	OSServiceId_Schedule,
	OSServiceId_SetAbsAlarm,
	OSServiceId_SetEvent,
	OSServiceId_SetRelAlarm,
	OSServiceId_ShutdownOS,
	OSServiceId_StartOS,
	OSServiceId_SuspendAllInterrupts,
	OSServiceId_SuspendOSInterrupts,
	OSServiceId_TerminateTask,
	OSServiceId_WaitEvent,
	/* AUTOSAR CP服务号 */
	OSServiceId_GetApplicationID,
	OSServiceId_GetISRID,
	OSServiceId_CallTrustedFunction,
	OSServiceId_CheckISRMemoryAccess,
	OSServiceId_CheckTaskMemoryAccess,
	OSServiceId_CheckObjectAccess,
	OSServiceId_CheckObjectOwnership,
	OSServiceId_StartScheduleTableRel,
	OSServiceId_StartScheduleTableAbs,
	OSServiceId_StopScheduleTable,
	OSServiceId_NextScheduleTable,
	OSServiceId_StartScheduleTableSynchron,
	OSServiceId_SyncScheduleTable,
	OSServiceId_SetScheduletableAsync,
	OSServiceId_GetScheduleTableStatus,
	OSServiceId_IncrementCounter,
	OSServiceId_GetCounterValue,
	OSServiceId_GetElapsedValue,
	OSServiceId_TerminateApplication,
	OSServiceId_AllowAccess,
	OSServiceId_GetApplicationState,
	OSServiceId_GetNumberOfActivatedCores,
	OSServiceId_GetCoreID,
	OSServiceId_StartCore,
	OSServiceId_StartNonAutosarCore,
	OSServiceId_GetSpinlock,
	OSServiceId_ReleaseSpinlock,
	OSServiceId_TryToGetSpinlock,
	OSServiceId_ShutdownAllCores,
	OSServiceId_ControlIdle,
	OSServiceId_ReadPeripheral8,
	OSServiceId_ReadPeripheral16,
	OSServiceId_ReadPeripheral32,
	OSServiceId_WritePeripheral8,
	OSServiceId_WritePeripheral16,
	OSServiceId_WritePeripheral32,
	OSServiceId_ModifyPeripheral8,
	OSServiceId_ModifyPeripheral16,
	OSServiceId_ModifyPeripheral32,
	OSServiceId_EnableInterruptSource,
	OSServiceId_DisableInterruptSource,
	OSServiceId_ClearPendingInterrupt,
	OSServiceId_ActivateTaskAsyn,
	OSServiceId_SetEventAsyn,

	/* 附加接口 */
	OSServiceId_GetSysLog,
} OSServiceIdType;

#endif /* OS_SYSTEM_TYPES_H */
