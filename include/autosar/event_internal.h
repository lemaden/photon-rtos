/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR事件内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_EVENT_INTERNAL_H
#define OS_EVENT_INTERNAL_H

#include <autosar/event.h>
#include <autosar/task_types.h>
#include <autosar/calllevel_internal.h>

FUNC(void, OS_CODE) __osek_set_event(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(EventMaskType, AUTOMATIC) Mask);

#endif /* OS_EVENT_INTERNAL_H */
