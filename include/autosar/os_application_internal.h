/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR应用内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_APPLICATION_INTERNAL_H
#define OS_APPLICATION_INTERNAL_H

#include <autosar/os_application.h>
#include <autosar/calllevel_internal.h>

#define MAX_TRUSTED_FUNC_NUM 10

typedef P2FUNC(void, OS_APPL_CODE, OsApplicationHook)(void);
typedef P2FUNC(void, OS_APPL_CODE, OsApplicationErrorHook)(VAR(StatusType, AUTOMATIC) code);

struct os_application {
		VAR(ApplicationType, TYPEDEF) app_id;
		VAR(int32_t, TYPEDEF) state;
		VAR(bool, TYPEDEF) if_trusted;
		VAR(CoreIdType, TYPEDEF) core_id;
		
		VAR(OsApplicationHook, TYPEDEF) startup_hook;
		VAR(OsApplicationHook, TYPEDEF) shutdown_hook;
		VAR(OsApplicationErrorHook, TYPEDEF) error_hook;
};

struct os_application_attr {
		VAR(OsApplicationHook, TYPEDEF) startup_hook;
		VAR(OsApplicationHook, TYPEDEF) shutdown_hook;
		VAR(OsApplicationErrorHook, TYPEDEF) error_hook;
};

struct autosar_trusted_function {
		void (*trusted_func)(TrustedFunctionIndexType, TrustedFunctionParameterRefType);
		/**
		 * 允许参数代表的 app 访问此 trusted_func.
		 */
		VAR(ApplicationType, TYPEDEF)
		accessable_application_id[AUTOSAR_NR_APPLICATION];
		/**
		 * 所从属的os-app的id.
		 */
		VAR(ApplicationType, TYPEDEF)
		app_id;
};

typedef void (*OsTrustedFuncRefType)(TrustedFunctionIndexType, TrustedFunctionParameterRefType);

typedef VAR(struct autosar_trusted_function, TYPEDEF) OsTrustedFuncType;

typedef VAR(struct os_application, TYPEDEF) OsOsAppType;
typedef P2VAR(OsOsAppType, TYPEDEF, OS_APPL_DATA) OsOsAppRefType;

extern VAR(OsOsAppType, AUTOMATIC) autosar_os_app[AUTOSAR_NR_APPLICATION];
#if defined(AUTOSAR_OS_APPLICATION)
extern struct os_application_attr autosar_os_app_attr[AUTOSAR_NR_APPLICATION];
#endif /* defined(AUTOSAR_OS_APPLICATION) */

#if defined(CONFIG_AUTOSAR_APPLICATION_HOOK_STARTUP)
extern void os_app_startup_hooks(void);
#endif
#if defined(CONFIG_AUTOSAR_APPLICATION_HOOK_SHUTDOWN)
extern void os_app_shutdown_hooks(void);
#endif
FUNC(void, OS_CODE) os_app_error_hooks(VAR(StatusType, AUTOMATIC) code);

extern VAR(OsTrustedFuncType, AUTOMATIC) autosar_trusted_func[MAX_TRUSTED_FUNC_NUM];

#if defined(AUTOSAR_OS_APPLICATION)
FUNC(void, OS_CODE) trusted_func_init(void);
FUNC(void, OS_CODE) TRUSTED_Test_Service1(
	VAR(TrustedFunctionIndexType, AUTOMATIC) func_index,
	VAR(TrustedFunctionParameterRefType, AUTOMATIC) param);
FUNC(void, OS_CODE) TRUSTED_Test_Service0(
	VAR(TrustedFunctionIndexType,AUTOMATIC) func_index,
	VAR(TrustedFunctionParameterRefType, AUTOMATIC) param);
FUNC(void, OS_CODE) application_init(void);
#endif /* defined(AUTOSAR_OS_APPLICATION) */
FUNC(void, OS_CODE) check_app_mode(void);

#endif /* OS_APPLICATION_INTERNAL_H */

