/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR通用类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_COMMON_TYPES_H
#define OS_COMMON_TYPES_H

#include <autosar/compiler.h>
#include <autosar/error.h>
#include <photon/types.h>

typedef VAR(signed char, TYPEDEF) int8;
typedef VAR(unsigned char, TYPEDEF) uint8;

typedef VAR(signed short, TYPEDEF) int16;
typedef VAR(unsigned short, TYPEDEF) uint16;

typedef VAR(signed int, TYPEDEF) int32;

#ifndef TYPE_UINT32
#define TYPE_UINT32
typedef VAR(unsigned int, TYPEDEF) uint32;
#endif

typedef VAR(signed long long, TYPEDEF) int64;
typedef VAR(unsigned long long, TYPEDEF) uint64;

typedef VAR(uint8, TYPEDEF) boolean;



#endif /* OS_COMMON_TYPES_H */
