/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR中断内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ISR_INTERNAL_H
#define OS_ISR_INTERNAL_H

#include <autosar/isr.h>
#include <photon/accurate_counter.h>
#include <autosar/calllevel_internal.h>

extern VAR(struct accurate_counter, AUTOMATIC) osek_all_interrupt_nesting[MAX_CPUS];
extern VAR(uintptr_t, AUTOMATIC) osek_all_interrupt_flags[MAX_CPUS];
extern VAR(struct accurate_counter, AUTOMATIC) osek_os_interrupt_nesting[MAX_CPUS];
extern VAR(uintptr_t, AUTOMATIC) osek_os_interrupt_flags[MAX_CPUS];
#endif /* OS_ISR_INTERNAL_H */
