/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR任务配置
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_TASK_CONFIG_H
#define OS_TASK_CONFIG_H

#define OS_NR_TASK CONFIG_OSEK_TASKS_COUNT

/**
 * TASK宏定义，开始定义一个任务的执行体
 *        name：任务名称
 */
#define TASK(Name, Section_Name)						\
	extern void OS_TASK_ENTRY_##Name(void) __attribute__((section("."#Section_Name".code")));	\
	void OS_TASK_ENTRY_##Name(void)

/**
 * DeclareTask宏定义，声明一个任务
 *		  Name：任务名称
 *		  Autostart：是否为自动启动任务
 *		  Prio：任务优先级
 *		  Flags：任务标志
 *		  AppMode：应用模式
 *		  CoreID：核心ID
 */
#define DeclareTask(Name, Autostart, Prio, Flags, AppMode, CoreID,	\
	TaskTimeFrame, IsrTimeFrame, IsrBudget, TaskBuddget, AppId, SchedPolicy)	\
	extern void OS_TASK_ENTRY_##Name(void);		\
	struct osek_task_attr OS_TASK_ATTR_##Name =		\
	{								\
		.entry =OS_TASK_ENTRY_##Name,			\
		.prio = Prio,						\
		.auto_start = Autostart,				\
		.flags = Flags,						\
		.app_mode = AppMode,					\
		.core_id = CoreID,					\
		.cp_task_time_frame = TaskTimeFrame,			\
		.cp_isr_time_frame = IsrTimeFrame,			\
		.cp_task_execution_budget = TaskBuddget,		\
		.cp_isr_execution_budget = IsrBudget,			\
		.app_id = AppId,						\
		.sched_policy = SchedPolicy \
	}

#endif
