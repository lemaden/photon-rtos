/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR调度表接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SCHED_TABLE_H
#define OS_SCHED_TABLE_H

#include <autosar/schedule_table_types.h>
#include <autosar/schedule_table_config.h>
#include <autosar/counter_types.h>

FUNC(int8 *, OS_CODE) ScheduleTableStatus2String(
	VAR(ScheduleTableStatusType, AUTOMATIC) status);

/**
 * 语法
 * StatusType StartScheduleTableRel (
 *      ScheduleTableType ScheduleTableID,
 *      TickType Offset
 * )
 *
 * 服务 ID [hex]
 *      0x07
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Schedule TableID
 *      待启动的调度表
 *
 * Offset
 *      在调度表处理开始之前计数器上的tick数
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID 无效。
 *      E_OS_VALUE（仅在 EXTENDED 状态下）：偏移量大于
 *	        （OsCounterMaxAllowedValue - InitialOffset）或等于 0。
 *      E_OS_STATE：调度表已经启动。
 *
 * 描述
 *      此服务在相对于基础计数器上的“Now”值的“Offset”处开始处理调度
 * 表。
 *
 * 引用：
 *      AUTOSAR OS 8.4.9 StartScheduleTableRel [SWS_Os_00347]
 */
FUNC(StatusType, OS_CODE) StartScheduleTableRel(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID,
	VAR(TickType, AUTOMATIC) Offset);

/*
 * 语法
 * StatusType StartScheduleTableAbs (
 *      ScheduleTableType ScheduleTableID,
 *      TickType Start
 * )
 *
 * 服务 ID [hex]
 *      0x08
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Schedule TableID
 *      待启动的调度表
 *      Start
 *      计划表开始时的绝对计数器刻度值
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID 无效
 *      E_OS_VALUE（仅处于 EXTENDED 状态）：“Start”的值比 OsCounterMaxAllowedValue
 * 更大
 *      E_OS_STATE：调度表已经启动了
 *
 * 描述
 *      该服务以基础计数器上的绝对值“Start”开始处理调度表。
 *
 * 引用
 *      AUTOSAR OS 8.4.10 StartScheduleTableAbs [SWS_Os_00358]
 */
FUNC(StatusType, OS_CODE) StartScheduleTableAbs(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID,
	VAR(TickType, AUTOMATIC) Start);

/*
 * 语法
 * StatusType StopScheduleTable (
 *      ScheduleTableType ScheduleTableID
 * )
 *
 * 服务 ID [hex]
 *      0x09
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID
 *      要停止的调度表
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID 无效。
 *      E_OS_NOFUNC：调度表已经停止
 *
 * 描述
 *      此服务会在调度表运行期间的任何时候立即取消对调度表的处理。
 *
 * 引用
 *      AUTOSAR OS 8.4.11 StopScheduleTable [SWS_Os_00006]
 */
FUNC(StatusType, OS_CODE) StopScheduleTable(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID);

/*
 * 语法
 * StatusType NextScheduleTable (
 *      ScheduleTableType ScheduleTableID_From,
 *      ScheduleTableType ScheduleTableID_To
 * )
 *
 * 服务 ID [hex]
 *      0x0a
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID_From
 *      当前处理的调度表
 *
 *      ScheduleTableID_To
 *      提供其一系列到期点的调度表
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：没有错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID_
 *      From 或 ScheduleTableID_To 无效
 *      E_OS_NOFUNC：ScheduleTableID_From 未启动
 *      E_OS_STATE：ScheduleTableID_To 已启动或下一个
 *
 * 描述
 *      该服务将处理从一个调度表切换到另一个调度表。
 *
 * 引用
 *      AUTOSAR OS 8.4.12 NextScheduleTable [SWS_Os_00191]
 */
FUNC(StatusType, OS_CODE) NextScheduleTable(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID_From,
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID_To);

#if defined(AUTOSAR_SYNCHRONIZATION_SUPPORT)
/*
 * 语法
 * StatusType StartScheduleTableSynchron (
 *      ScheduleTableType ScheduleTableID
 * )
 *
 * 服务 ID [hex]
 *      0x0b
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID
 *      计划表开始
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID 无效
 *      E_OS_STATE：调度表已经启动
 *
 * 描述
 *      此服务同步启动显式同步的调度表。
 *
 * 引用
 *      AUTOSAR OS 8.4.13 StartScheduleTableSynchron [SWS_Os_00201]
 */
FUNC(StatusType, OS_CODE) StartScheduleTableSynchron(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID);

/*
 * 语法
 * StatusType SyncScheduleTable (
 *      ScheduleTableType ScheduleTableID,
 *      TickType Value
 * )
 *
 * 服务 ID [hex]
 *      0x0c
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID
 *      要同步的调度表
 *
 *      Value
 *      同步计数器的当前值
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：没有错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：ScheduleTableID 无效或调度
 *      表无法同步（未设置 OsScheduleTblSyncStrategy 或
 *      OsScheduleTblSyncStrategy = 隐式（IMPLICIT））
 *      E_OS_VALUE（仅处于 EXETENDED 状态）：<Value> 超出范围
 *      E_OS_STATE：调度表<ScheduleTableID>的状态等于
 *      SCHEDULETABLE_STOPPED
 *
 * 描述
 *      该服务为调度表提供同步计数和开始同步。
 *
 * 引用
 *      AUTOSAR OS 8.4.14 SyncScheduleTable [SWS_Os_00199]
 */
FUNC(StatusType, OS_CODE) SyncScheduleTable(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID,
	VAR(TickType, AUTOMATIC) Value);

/*
 * 语法
 * StatusType SetScheduleTableAsync (
 *      ScheduleTableType ScheduleTableID
 * )
 *
 * 服务 ID [hex]
 *      0x0d
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID
 *      请求切换状态的调度表ID
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：无效的 Schedule TableID
 *
 * 描述
 *      此服务停止计划表的同步。
 *
 * 引用
 *      AUTOSAR OS 8.4.15 SetScheduleTableAsync [SWS_Os_00422]
 */
FUNC(StatusType, OS_CODE) SetScheduleTableAsync(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID);
#endif /* defined(AUTOSAR_SYNCHRONIZATION_SUPPORT) */

/*
 * 语法
 * StatusType GetScheduleTableStatus (
 *      ScheduleTableType ScheduleTableID,
 *      ScheduleTableStatusRefType ScheduleStatus
 * )
 *
 * 服务 ID [hex]
 *      0x0e
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      ScheduleTableID
 *      请求状态的调度表
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      ScheduleStatus
 *      ScheduleTableStatusType 的引用
 *
 * 返回值
 *      StatusType
 *      E_OK：无错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：无效的 Schedule TableID
 *
 * 描述
 *      该服务查询调度表的状态（也与同步相关）。
 *
 * 引用
 *      AUTOSAR OS 8.4.16 GetScheduleTableStatus [SWS_Os_00227]
 */
FUNC(StatusType, OS_CODE) GetScheduleTableStatus(
	VAR(ScheduleTableType, AUTOMATIC) ScheduleTableID,
	VAR(ScheduleTableStatusRefType, AUTOMATIC) ScheduleStatus);

#endif /* OS_SCHED_TABLE_H */
