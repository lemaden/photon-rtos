/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR资源接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_RESOURCE_H
#define OS_RESOURCE_H

#include <autosar/resource_types.h>
#include <autosar/resource_config.h>

/**
 * 该调用用于在代码中进入临界区，该临界区由<ResID>引用的资源确定。
 * 这样的临界区应当总是通过ReleaseResource来退出。
 * 语法：
 *     StatusType GetResource ( ResourceType <ResID> )
 * 参数（输入）：
 *     ResID			 资源引用
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     请参考  OSEK资源管理优先级上限协议。
 *     仅当内部临界区完全在surrounding临界区内
 *     执行时才允许嵌套资源占用。
 *     针对同一个资源的嵌套占用也是禁止的！
 *     建议对 GetResource 和 ReleaseResource 的
 *     相应调用出现在同一个函数中。
 *     在非抢占任务的重调度点
 *     （TerminateTask，ChainTask, Schedule 以及 WaitEvent），
 *     不允许使用该服务。
 *     另外，在中断服务程序完成之前要离开临界区。
 *     通俗的说，临界应该短小。
 *     该服务可以在ISR和任务级调用
 * 状态：
 *     标准：
 *         无错误，E_OK
 *     扩展：
 *         资源 <ResID> 无效，E_OS_ID
 *         试图获得一个资源，
 *         而该资源已经被某个任务或者ISR获得
 *         或者调用任务（及中断）的静态分配优先级高于
 *         计算的上限优先级，E_OS_ACCESS
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) GetResource(
	VAR(ResourceType, AUTOMATIC) ResID);

/**
 * ReleaseResource是与GetResource相对的调用
 * 用于退出由<ResID>引用所确定的代码临界区。
 * 语法：
 *     StatusType ReleaseResource ( ResourceType <ResID> )
 * 参数（输入）：
 *     ResID			资源引用
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     关于嵌套条件的相关信息，参见GetResource。
 *     该服务可以在ISR和任务级调用（参见图12.1）
 * 状态：
 *     标准：
 *        无错误，E_OK
 *     扩展：
 *        资源<ResID>无效，E_OS_ID
 *        试图释放并不由任务或者ISR占据的资源，
 *        或者在此之前需要释放另一个资源E_OS_NOFUNC
 *        试图释放比调用任务或者中断例程
 *        静态分配优先级更低上限优先级的资源，E_OS_ACCESS
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) ReleaseResource(
	VAR(ResourceType, AUTOMATIC) ResID);

#endif /* OS_RESOURCE_H */
