/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR任务类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_TASK_TYPES_H
#define OS_TASK_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint8, TYPEDEF) TaskType;
typedef P2VAR(TaskType, TYPEDEF,OS_APPL_DATA) TaskRefType;
typedef VAR(uint8, TYPEDEF)	TaskStateType;
typedef P2VAR(TaskStateType, TYPEDEF,OS_APPL_DATA) TaskStateRefType;

#endif /* OS_TASK_TYPES_H */
