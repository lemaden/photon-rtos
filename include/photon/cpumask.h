/*
 * PhotonRTOS础光实时操作系统 -- CPU掩码头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_CPUMASK_H
#define PHOTON_CPUMASK_H

#include <photon/bitmap.h>
#include <photon/kernel.h>
#include <photon/cpu.h>

typedef struct cpumask { DECLARE_BITMAP(bits, MAX_CPUS); } cpumask_t;

#define cpumask_bits(maskp) ((maskp)->bits)

extern const struct cpumask *const cpu_possible_mask;
extern const struct cpumask *const cpu_online_mask;
extern const struct cpumask *const cpu_present_mask;

#define num_online_cpus()	cpumask_weight(cpu_online_mask)
#define num_possible_cpus()	cpumask_weight(cpu_possible_mask)
#define num_present_cpus()	cpumask_weight(cpu_present_mask)
#define cpu_online(cpu)		cpumask_test_cpu((cpu), cpu_online_mask)
#define cpu_possible(cpu)	cpumask_test_cpu((cpu), cpu_possible_mask)
#define cpu_present(cpu)	cpumask_test_cpu((cpu), cpu_present_mask)

static inline uint32_t cpumask_check(uint32_t cpu)
{
	return cpu;
}

static inline uint32_t cpumask_first(const struct cpumask *srcp)
{
	return find_first_bit(cpumask_bits(srcp), MAX_CPUS);
}

static inline uint32_t cpumask_next(int32_t n, const struct cpumask *srcp)
{
	if (n != -1) {
		cpumask_check(n);
	}
	return find_next_bit(cpumask_bits(srcp), MAX_CPUS, n+1);
}

#define for_each_cpu(cpu, mask)				\
	for ((cpu) = -1;				\
		(cpu) = cpumask_next((cpu), (mask)),	\
		(cpu) < MAX_CPUS;)


#define CPU_BITS_NONE					\
{							\
	[0 ... BITS_TO_LONGS(MAX_CPUS)-1] = 0UL		\
}

#define CPU_BITS_CPU0					\
{							\
	[0] =  1UL					\
}

static inline void cpumask_set_cpu(uint32_t cpu, struct cpumask *dstp)
{
	atomic_set_bit(cpumask_check(cpu), cpumask_bits(dstp));
}

static inline void cpumask_clear_cpu(int32_t cpu, struct cpumask *dstp)
{
	atomic_clear_bit(cpumask_check(cpu), cpumask_bits(dstp));
}

#define cpumask_test_cpu(cpu, cpumask)			\
	test_bit(cpumask_check(cpu), cpumask_bits((cpumask)))

static inline uint32_t cpumask_weight(const struct cpumask *srcp)
{
	return bitmap_weight(cpumask_bits(srcp), MAX_CPUS);
}

static inline void cpumask_copy(struct cpumask *dstp,
				const struct cpumask *srcp)
{
	bitmap_copy(cpumask_bits(dstp), cpumask_bits(srcp), MAX_CPUS);
}

#define for_each_possible_cpu(cpu) for_each_cpu((cpu), cpu_possible_mask)
#define for_each_online_cpu(cpu)   for_each_cpu((cpu), cpu_online_mask)
#define for_each_present_cpu(cpu)  for_each_cpu((cpu), cpu_present_mask)

void mark_cpu_possible(uint32_t cpu, bool possible);
void mark_cpu_present(uint32_t cpu, bool present);
void mark_cpu_online(uint32_t cpu, bool online);
void init_cpu_present(const struct cpumask *src);
void init_cpu_possible(const struct cpumask *src);
void init_cpu_online(const struct cpumask *src);

#define to_cpumask(bitmap)					\
	((struct cpumask *)(1 ? (bitmap)			\
		: (void *)sizeof(__check_is_bitmap(bitmap))))

static inline int32_t __check_is_bitmap(const uintptr_t *bitmap)
{
	return 1;
}

#define cpu_is_offline(cpu)	unlikely(!cpu_online(cpu))

#if MAX_CPUS <= BITS_PER_LONG
#define CPU_BITS_ALL						\
{								\
	[BITS_TO_LONGS(MAX_CPUS)-1] = CPU_MASK_LAST_WORD	\
}

#else /* MAX_CPUS > BITS_PER_LONG */

#define CPU_BITS_ALL						\
{								\
	[0 ... BITS_TO_LONGS(MAX_CPUS)-2] = ~0UL,		\
	[BITS_TO_LONGS(MAX_CPUS)-1] = CPU_MASK_LAST_WORD	\
}
#endif /* MAX_CPUS > BITS_PER_LONG */

#define cpu_possible_map	(*(cpumask_t *)cpu_possible_mask)
#define cpu_online_map		(*(cpumask_t *)cpu_online_mask)
#define cpu_present_map		(*(cpumask_t *)cpu_present_mask)

#define CPU_MASK_LAST_WORD BITMAP_LAST_WORD_MASK(MAX_CPUS)

#endif /* PHOTON_CPUMASK_H */
