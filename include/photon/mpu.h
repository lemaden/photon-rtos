/*
 * PhotonRTOS础光实时操作系统 -- 魔法数常量
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef _OS_MPU_H
#define _OS_MPU_H
extern struct task_address_range task_address_range[];

extern void mpu_init();
#endif
