/*
 * PhotonRTOS础光实时操作系统 -- 时间相关头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_GENERIC_TIMEX_H
#define __ASM_GENERIC_TIMEX_H

#include <asm/timex.h>

typedef unsigned long cycles_t;
#ifndef get_cycles
/* arm架构下的R核有定义,M核未定义 */
static inline cycles_t get_cycles(void)
{
	return 0;
}
#endif

#endif /* __ASM_GENERIC_TIMEX_H */
