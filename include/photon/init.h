/*
 * PhotonRTOS础光实时操作系统 -- 启动相关头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _PHOTON_INIT_H
#define _PHOTON_INIT_H

#include <photon/kernel.h>


#ifndef __ASSEMBLY__

/**
 * 系统启动状态
 */
enum boot_states {
	/**
	 * 正在开始启动
	 */
	BOOTING,
	/**
	 * 早期启动
	 */
	EARLY_INIT,
	/**
	 * 主体初始化完毕
	 * 准备正式运行
	 */
	KERN_PREPARE_RUN,
	/**
	 * 启动完毕，正常运行
	 */
	KERN_RUNNING,
};
extern enum boot_states boot_state;

void start_arch(void);
void init_time(void);

asmlinkage void start_master(void);

extern int32_t boot_step;
#endif

#endif /* _PHOTON_INIT_H */
