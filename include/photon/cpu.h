/*
 * PhotonRTOS础光实时操作系统 -- CPU相关头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_CPU_H
#define PHOTON_CPU_H

#include <photon/init.h>

/**
 * 主核在启动从核时，传递给从核的参数
 */
struct slave_cpu_data {
	/**
	 * 从CPU堆栈，必须是第一个字段
	 */
	void *stack;
	/**
	 * 从CPU的ID
	 */
	int32_t cpu;
};

#define MAX_CPUS		CONFIG_MAX_CPUS

extern int32_t osek_core_if_activate[MAX_CPUS];

extern int32_t osek_core_if_set_event[MAX_CPUS];

extern int32_t arch_launch_cpu(uint32_t cpu);

/**
 * 启动CPU开始工作
 */
int32_t cpu_launch(uint32_t cpu);

/**
 * 初始化主核及从核的C入口函数
 */
asmlinkage void start_master(void);
asmlinkage void start_slave(struct slave_cpu_data *);

#endif /* PHOTON_CPU_H */
