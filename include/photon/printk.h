/*
 * PhotonRTOS础光实时操作系统 -- 打印
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_PRINTK_H
#define _OS_PRINTK_H

#include <photon/types.h>

int printk(const char *fmt, ...);

#ifndef KBUILD_MODNAME
#define KBUILD_MODNAME "os"
#endif

#ifndef pr_fmt
#define pr_fmt(fmt) "/" KBUILD_MODNAME "] " fmt
#endif

#define	KERN_EMERG		"<0>"
#define	KERN_ALERT		"<1>"
#define	KERN_CRIT		"<2>"
#define	KERN_ERR		"<3>"
#define	KERN_WARNING	"<4>"
#define	KERN_NOTICE		"<5>"
#define	KERN_INFO		"<6>"
#define	KERN_DEBUG		"<7>"

#define KERN_DEFAULT	KERN_DEBUG
#define KERN_LOGLEVEL		7

#define pr_emerg(fmt, ...) \
	printk(KERN_EMERG "[EMERG" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_alert(fmt, ...) \
	printk(KERN_ALERT "[ALERT" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_crit(fmt, ...) \
	printk(KERN_CRIT "[CRIT" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_err(fmt, ...) \
	printk(KERN_ERR "[E" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warning(fmt, ...) \
	printk(KERN_WARNING "[W" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warn pr_warning
#define pr_notice(fmt, ...) \
	printk(KERN_NOTICE "[NOTICE" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_info(fmt, ...) \
	printk(KERN_INFO "[I" pr_fmt(fmt), ##__VA_ARGS__)
#define pr_debug(fmt, ...) \
	printk(KERN_DEBUG "[D" pr_fmt(fmt), ##__VA_ARGS__)

void panic(const char *fmt, ...);
void print_warning(const char *file, const int32_t line);
uint32_t dmesg(char buf[], uint32_t size, bool buf_clear);

#endif /* _OS_PRINTK_H */
