/*
 * PhotonRTOS础光实时操作系统 -- 多核
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef PHOTON_SMP_H
#define PHOTON_SMP_H

#include <photon/init.h>
#include <photon/types.h>
#include <photon/double_list.h>
#include <photon/cpumask.h>
#include <photon/preempt.h>
#include <photon/kernel.h>
#include <photon/process.h>
#include <asm/smp.h>
#include <photon/smp_lock.h>

/**
 * IPI命令类型
 */
enum ipi_cmd_type {
	/**
	 * AutosarCP要求其他核执行关机操作，关闭核心
	 */
	IPI_SHUTDOWN,
	/**
	 * AutosarCP IOC通讯出发接受回调
	 */
	IPI_IOC,
	/**
	 * AutosarCP 核减异步机制
	 */
	IPI_ASYNC,
};

struct smp_call_data {
	struct smp_lock lock;
	void (*func)(void *info);
	void *priv;
	uint16_t flags;
};

int32_t smp_call_function(void(*func)(void *info), void *info, int32_t wait);
int32_t smp_call_for_all(void (*func) (void *info), void *info, int32_t wait);

#define get_cpu()		({ preempt_disable(); smp_processor_id(); })
#define put_cpu()		preempt_enable()

static int get_core_id(void)
{
	return arch_get_core_id();
}

extern void slave_cpu_entry(void);

extern void launch_slave(void);
void init_slave_early(void);

#endif /* PHOTON_SMP_H */
