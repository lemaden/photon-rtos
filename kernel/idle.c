/*
 * 础光实时操作系统PhotonRTOS -- idle处理框架
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/kernel.h>
#include <photon/sched.h>
#include <autosar/internal.h>
#include <asm/barrier.h>

void (*powersave)(void) = NULL;
static void default_powersave(void)
{
	VAR(CoreIdType, AUTOMATIC) CoreID;

	CoreID = GetCoreID();
	if (autosar_get_idle_mode(CoreID) != IDLE_NO_HALT) {
		cpu_do_idle();
	}
}

static void default_idle(void)
{
	disable_irq();
	if (!need_resched()) {
		if (powersave != NULL) {
			powersave();
		}
		else {
			default_powersave();
		}
	}
	enable_irq();
}

void cpu_idle(void)
{
	while (true) {
		void (*idle)(void);

		/* 这里的读屏障是需要的，
		因为其他核可能触发本CPU的调度 */
		rmb();
		idle = default_idle;


		preempt_disable();
		while (!need_resched()) {
			idle();
		}

		preempt_enable();
		schedule();
	}
}
