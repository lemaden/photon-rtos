/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR内存检查
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <autosar/internal.h>

#if defined(AUTOSAR_MEMORY_PROTECTION)
FUNC(AccessType, OS_CODE) autosar_check_isr_memory_access(
	VAR(ISRType, AUTOMATIC) ISRID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size)
{
	VAR(uintptr_t, AUTOMATIC) addr = (VAR(uintptr_t, AUTOMATIC))Address;
	/**
	 * [SWS_Os_00268] 如果 ISR 引用 <ISRID> 无效， CheckISRMemoryAccess()将返回无访问权限。 ⌋ ( )
	 */
	if (ISRID < 0 || ISRID >= NR_IRQS) {
		return OSMEMORY_IS_UNACCESSABLE;
	}
	/**
	 * [SWS_Os_00267] ⌈如果	ISR 引用 <ISRID> 在调用CheckISRMemoryAccess()时有效， CheckISRMemoryAccess()将返回 ISR 对指定内存区域的访问权限。 ⌋ ( )
	 */
	if (addr >= (uintptr_t)osek_tasks[ISRID].cp_attr.prot_addr_start
		&& addr <= (uintptr_t)osek_tasks[ISRID].cp_attr.prot_addr_end - Size) {
		return OSMEMORY_IS_READABLE | OSMEMORY_IS_WRITEABLE;
	}
	/**
	 * [SWS_Os_00313] ⌈如果访问权限（例如“读取”）对于在CheckISRMemoryAccess()调用中指定的整个内存区域无效，则CheckISRMemoryAccess()将不会产生与此权限相关的访问权限。 ⌋ ( )
	 */
	return OSMEMORY_IS_UNACCESSABLE;
}

FUNC(AccessType, OS_CODE) autosar_check_task_memory_access(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size)
{
	VAR(uintptr_t, AUTOMATIC) addr = (VAR(uintptr_t, AUTOMATIC))Address;

	/**
	 * [SWS_Os_00268] 如果 TASK 引用 <TASKID> 无效， CheckTASKMemoryAccess()将返回无访问权限。 ⌋ ( )
	 */
	if (TaskID < 0 || TaskID >=OS_NR_TASK) {
		return OSMEMORY_IS_UNACCESSABLE;
	}
//
//	/**
//	 * [SWS_Os_00267] ⌈如果	TASK 引用 <TASKID> 在调用CheckTASKMemoryAccess()时有效， CheckTASKMemoryAccess()将返回 TASK 对指定内存区域的访问权限。 ⌋ ( )
//	 */
	if (addr >= (uintptr_t)osek_tasks[TaskID].cp_attr.prot_addr_start
		&& addr <= (uintptr_t)osek_tasks[TaskID].cp_attr.prot_addr_end - Size) {
		return OSMEMORY_IS_READABLE | OSMEMORY_IS_WRITEABLE;
	}

	/**
	 * [SWS_Os_00268] 如果 TASK 引用 <TASKID> 无效， CheckTASKMemoryAccess()将返回无访问权限。 ⌋ ( )
	 */
	return OSMEMORY_IS_UNACCESSABLE;
}

/**
 * [SWS_Os_00517] ⌈ CheckISRMemoryAccess()的可用性：在可扩展性等级 3 和 4 中可用。⌋ ( )
 */
FUNC(AccessType, OS_CODE) CheckISRMemoryAccess(
	VAR(ISRType, AUTOMATIC) ISRID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size)
{
	CONTEXT_CHECK(ENV_TASK | ENV_CAT2_ISR | ENV_ERROR_HOOK | ENV_PROTECTION_HOOK)
	return autosar_check_isr_memory_access(ISRID, Address, Size);
}

FUNC(AccessType, OS_CODE) CheckTaskMemoryAccess(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(MemoryStartAddressType, AUTOMATIC) Address,
	VAR(MemorySizeType, AUTOMATIC) Size)
{
	CONTEXT_CHECK(ENV_TASK | ENV_CAT2_ISR | ENV_ERROR_HOOK | ENV_PROTECTION_HOOK)
	return autosar_check_task_memory_access(TaskID, Address, Size);
}
#endif /* defined(AUTOSAR_MEMORY_PROTECTION) */
