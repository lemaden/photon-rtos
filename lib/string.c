/*
 * 础光实时操作系统PhotonRTOS -- 位查找实现文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/types.h>
#include <photon/string.h>
#include <photon/bug_build.h>
#ifndef __HAVE_ARCH_STRCPY
/**
 * strcpy - Copy a %NUL terminated string
 * @dest: Where to copy the string to
 * @src: Where to copy the string from
 */
#undef strcpy
char *strcpy(char *dest, char *src)
{
	char *dest_t = dest;
	char *src_t = src;

	while ((*dest_t = *src_t) != '\0')
	{
		dest_t++;
		src_t++;
	}
	return dest;
}
#endif

#ifndef __HAVE_ARCH_STRCMP
/**
 * strcmp - Compare two strings
 * @cs: One string
 * @ct: Another string
 */
#undef strcmp
int32_t strcmp(const char *cs, const char *ct)
{
	uint8_t c1, c2;

	while (true) {
		c1 = *cs;
		c2 = *ct;
		cs++;
		ct++;
		if (c1 != c2) {
			return c1 < c2 ? -1 : 1;
		}
		if (!c1) {
			break;
		}
	}
	return 0;
}
#endif

#ifndef __HAVE_ARCH_STRNCMP
/**
 * strncmp - Compare two length-limited strings
 * @cs: One string
 * @ct: Another string
 * @count: The maximum number of bytes to compare
 */
int32_t strncmp(const char *cs, const char *ct, size_t count)
{
	uint8_t c1, c2;

	while (count != 0) {
		c1 = *cs;
		c2 = *ct;
		cs++;
		ct++;
		if (c1 != c2) {
			return c1 < c2 ? -1 : 1;
		}
		if (!c1) {
			break;
		}
		count--;
	}
	return 0;
}
#endif
static inline int32_t isspace_(char c)
{
	int32_t ret = 0;

	if(c =='\t'|| c =='\n'|| c ==' ' || c == '\v' || c == '\f' || c == '\r') {
		ret = 1;
	}

	return ret;
}
/**
 * skip_spaces - Removes leading whitespace from @str.
 * @str: The string to be stripped.
 *
 * Returns a pointer to the first non-whitespace character in @str.
 */
char *skip_spaces(const char *str)
{
	while (isspace_(*str) != 0) {
		++str;
	}
	return (uint8_t *)str;
}


#ifndef __HAVE_ARCH_STRLEN
/**
 * strlen - Find the length of a string
 * @s: The string to be sized
 */
size_t strlen(const char *s)
{
	const char *sc;

	for (sc = s; *sc != '\0'; ++sc)
	{
		/* nothing */;
	}
	return sc - s;
}
#endif

#ifndef __HAVE_ARCH_STRNLEN
/**
 * strnlen - Find the length of a length-limited string
 * @s: The string to be sized
 * @count: The maximum number of bytes to search
 */
size_t strnlen(const char *s, size_t count)
{
	const char *sc;
	size_t count_t = count;

	for (sc = s; count_t-- && *sc != '\0'; ++sc)
	{
		/* nothing */;
	}
	return sc - s;
}
#endif


#ifndef __HAVE_ARCH_MEMSET
/**
 * memset - Fill a region of memory with the given value
 * @s: Pointer to the start of the area.
 * @c: The byte to fill the area with
 * @count: The size of the area.
 *
 * Do not use memset() to access IO space, use memset_io() instead.
 */
void *memset(void *s, int32_t c, size_t count)
{
	char *xs = s;
	size_t count_t = count;

	while (count_t != 0UL) {
		count_t--;
		*xs = c;
		xs++;
	}
	return s;
}
#endif

#ifndef __HAVE_ARCH_MEMCPY
/**
 * memcpy - Copy one area of memory to another
 * @dest: Where to copy to
 * @src: Where to copy from
 * @count: The size of the area.
 *
 * You should not use this function to access IO space, use memcpy_toio()
 * or memcpy_fromio() instead.
 */
void *memcpy(void *dest, const void *src, size_t count)
{
	char *tmp = dest;
	const char *src_t = src;
	size_t count_t = count;

	while (count_t != 0UL) {
		count_t--;
		*tmp = *src_t;
		tmp++;
		src_t++;
	}
	return dest;
}
#endif

