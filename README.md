# PhotonRTOS础光车控安全实时操作系统

PhotonRTOS是一个由国科础石研发，并且符合AUTOSAR CP R20-11规范的实时操作系统，
它用于车辆底盘控制，汽车状态监控等场景。

## 已支持的功能

* 任务管理
   - 任务激活，任务终止
   - 全抢占式任务调度
* 中断处理
* 事件机制
* 资源管理(天花板协议等)
* Alarm
* Hook routines
* 调度表
* os-app
* 保护机制
   - 时间保护
   - 内存保护
   - 堆栈监控
   - 服务保护
   - 保护错误处理
* 多核相关
   - 多核启动
   - 自旋锁
* 硬件外设访问操作
* 中断源相关接口

## 代码结构

| 目录名  | 目录内容                                                   |
| ------- | ---------------------------------------------------------- |
| apps    | 包含了调用autosar cp接口实现的应用任务                     |
| arch    | 与不同架构相关的代码                                       |
| autosar | 按照OS_R20-11的规范实现的cp代码                            |
| include | 包含了各种头文件                                           |
| doc     | 包含了各种文档描述                                          |
| init    | 包含了内核由汇编跳转到c 的第一个文件，和一些初始化相关代码 |
| kernel  | 内核相关代码                                               |
| lib     | 一些工具类的代码                                           |
| scripts | 构建脚本，配置脚本等                                       |

## 平台支持

| 架构(arch)    | 平台(mach)   |
| ------- | ---------- |
| arm     | zynqmp_r5  |
| arm     | mps2_m3  |
| tricore     | tc397  |

## 开发流程

### 1. 配置依赖工具(开发准备工作，第一次运行一下即可，包括下载工具链等)

```bash
# 1. 如果没有工具链，可以先下载toolchains
# git clone --depth=1 https://gitee.com/kernelsoft/toolchains.git ../toolchains
bash quickstart.sh devel

# 2. 安装qemu
sudo apt install qemu-system-arm
```

### 2. 编译与运行

```bash
默认(arm, mps2_m3)平台：
bash quickstart.sh defconfig(把默认的Kconfig配置复制到.config文件中)
bash quickstart.sh

zynqmp_r5平台：
ARCH=arm MACH=zynqmp_r5 bash quickstart.sh defconfig
ARCH=arm MACH=zynqmp_r5 bash quickstart.sh

mps2_m3平台：
ARCH=arm MACH=mps2_m3 bash quickstart.sh defconfig
ARCH=arm MACH=mps2_m3 bash quickstart.sh
```

### 3. 调试支持

```bash
# 1. 安装调试工具gdb-multiarch
sudo apt install gdb-multiarch

# 2. 以gdb调试方式运行(通过ARCH和MACH参数来选择要调试的平台)
bash quickstart.sh gdb

# 3. 新建一个终端，调试elf格式的内核
gdb-multiarch ./photon

# 4. 进入gdb后，链接到qemu，就可以开始输入调试命令了
target remote:1234
```